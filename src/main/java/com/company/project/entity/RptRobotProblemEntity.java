package com.company.project.entity;

import lombok.Data;

@Data
public class RptRobotProblemEntity {

    // id
    private Integer id;

    // 问题
    private String problem;

    // 答案
    private String answer;

    // 预留字段1
    private String text1;

    // 预留字段2
    private String text2;

    // 预留字段3
    private String text3;
}
