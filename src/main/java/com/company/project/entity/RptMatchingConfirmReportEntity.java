package com.company.project.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 匹配结果手工确认表实体类
 */
@Data
@TableName("rpt_matching_confirm_report")
public class RptMatchingConfirmReportEntity extends BaseEntity implements Serializable {

    //ID
    @TableId
    private Integer id;

    //企业名称
    @TableField("enterprise_name")
    private String enterpriseName;

    //统一社会信用代码
    @TableField("social_credit_code")
    private String socialCreditCode;

    //管辖机关
    @TableField("domination")
    private String domination;

    //地址
    @TableField("address")
    private String address;

    //当前创建时间
    @TableField("create_time")
    private String createTime;

    //预留字段一
    @TableField("text1")
    private String text1;

    //预留字段二
    @TableField("text2")
    private String text2;

    //预留字段三
    @TableField("text3")
    private String text3;


    //当前创建时间--开始日期
    @TableField(exist = false)
    private String createStartTime;

    //当前创建时间--结束日期
    @TableField(exist = false)
    private String createEndTime;





}
