package com.company.project.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 年度统计报表
 */
@Data
@TableName("rpt_statistics_report")
public class    RptStatisticsReportEntity extends BaseEntity implements Serializable {

    //序号
    @TableId
    private Integer id;

    // 地址名称
    @TableField("address_name")
    private String addressName;

    // 企业类型
    @TableField("enterprise_type")
    private Integer enterpriseType;

    // 年报数量
    @TableField("annual_report")
    private Integer annualReport;

    // 提交年报的数量
    @TableField("reported_quantity")
    private Integer reportedQuantity;

    // 未提交年报的数量
    @TableField("not_report_quantity")
    private Integer notReportQuantity;

    // 年报率
    @TableField("annual_report_rate")
    private String annualReportRate;

    // 排名
    @TableField("ranking")
    private Integer ranking;

    //空号
    @TableField("empty_number")
    private Integer emptyNumber;

    //清吊
    @TableField("clear_number")
    private Integer clearNumber;

    //创建时间
    @TableField("create_time")
    private String createTime;

    //状态：1-表示工商统计 2-表示工商统计（包含清吊和空号）
    @TableField("status")
    private Integer status;

    //预留字段一
    @TableField("text1")
    private String text1;

    //预留字段一
    @TableField("text2")
    private String text2;

    //预留字段一
    @TableField("text3")
    private String text3;

    //当前创建时间--开始日期
    @TableField(exist = false)
    private String createStartTime;

    //当前创建时间--结束日期
    @TableField(exist = false)
    private String createEndTime;


}
