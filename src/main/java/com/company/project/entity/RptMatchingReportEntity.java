package com.company.project.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 *工商报表匹配结果表
 */
@Data
@TableName("rpt_matching_report")
public class RptMatchingReportEntity extends BaseEntity implements Serializable {

    //ID
    @TableId
    private Integer id;

    //企业名称
    @TableField("enterprise_name")
    private String enterpriseName;

    //统一社会信用代码
    @TableField("social_credit_code")
    private String socialCreditCode;

    //注册号
    @TableField("registration_number")
    private String registrationNumber;

    //管辖机关
    @TableField("domination")
    private String domination;

    //地址
    @TableField("address")
    private String address;

    //联系电话
    @TableField("contact_number")
    private String contactNumber;

    //成立日期
    @TableField("establish_date")
    private String establishDate;

    //核准日期
    @TableField("approval_date")
    private String approvalDate;

    //企业状态
    @TableField("enterprise_status")
    private String enterpriseStatus;

    //联络员姓名
    @TableField("liaison_man_name")
    private String liaisonManName;

    //联络员电话
    @TableField("liaison_man_phone")
    private String liaisonManPhone;

    //当前创建时间
    @TableField("create_time")
    private String createTime;

    //数据上报状态--1.已上报--2.未上报
    @TableField("report_status")
    private Integer reportStatus;

    //企业类型--1.企业--2.农合社--3.个人
    @TableField("enterprise_type")
    private Integer enterpriseType;

    //匹配结果--1.匹配成功--2.匹配失败
    @TableField("matching_result")
    private Integer matchingResult;

    //确认结果--1.已确认--2.未确认
    @TableField("confirm_status")
    private Integer confirmStatus;



    //成立日期--开始日期
    @TableField(exist = false)
    private String establishStartTime;

    //成立日期--结束日期
    @TableField(exist = false)
    private String establishEndTime;

    //核准日期--开始日期
    @TableField(exist = false)
    private String approvalStartTime;

    //核准日期--结束日期
    @TableField(exist = false)
    private String approvalEndTime;

    //当前创建时间--开始日期
    @TableField(exist = false)
    private String createStartTime;

    //当前创建时间--结束日期
    @TableField(exist = false)
    private String createEndTime;

    //匹配度
    @TableField(exist = false)
    private String degree;


}
