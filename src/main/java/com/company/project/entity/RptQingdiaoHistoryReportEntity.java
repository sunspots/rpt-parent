package com.company.project.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 *税务清吊历史报表
 */
@Data
@TableName("rpt_qingdiao_history_report")
public class RptQingdiaoHistoryReportEntity extends BaseEntity implements Serializable {

    //ID
    @TableId
    private Integer id;

    //企业名称
    @TableField("enterprise_name")
    private String enterpriseName;

    //统一社会信用代码
    @TableField("social_credit_code")
    private String socialCreditCode;

    //法定代表人
    @TableField("legal_representative")
    private String legalRepresentative;

    //经营场所
    @TableField("business_place")
    private String businessPlace;

    //经营范围
    @TableField("business_scope")
    private String businessScope;

    //联系人
    @TableField("contacts")
    private String contacts;

    //联系方式
    @TableField("contact_information")
    private String contactInformation;

    //状态
    @TableField("status")
    private String status;

    //数据上报状态--1.空号--2.税务上报异常--3.黑名单
    @TableField("report_status")
    private Integer reportStatus;

    //当前创建时间
    @TableField("create_time")
    private String createTime;

    //预留字段一
    @TableField("text1")
    private String text1;

    //预留字段二
    @TableField("text2")
    private String text2;

    //预留字段三
    @TableField("text3")
    private String text3;

    //当前创建时间--开始日期
    @TableField(exist = false)
    private String createStartTime;

    //当前创建时间--结束日期
    @TableField(exist = false)
    private String createEndTime;



}
