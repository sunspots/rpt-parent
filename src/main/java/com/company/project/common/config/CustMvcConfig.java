package com.company.project.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.List;

//在springboot框架的项目中使用了ResponseEntity<byte[]>来下载excel文件，遇到了下载内容是一堆英文字母乱码的问题。仔细看了下，应该是下载的内容byte 都变成base64了。
////	解决办法：加一个配置类，如下：
//@Configuration
public class CustMvcConfig extends WebMvcConfigurationSupport {

    @Override
    protected void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        super.configureMessageConverters(converters);
        converters.add(new ByteArrayHttpMessageConverter()); // ByteArrayHttpMessageConverter处理byte数据
    }

    @Bean(name = {"configure2","configure2"})
    public CustMvcConfig custMvcConfig(){
        return new CustMvcConfig();
    }


}
