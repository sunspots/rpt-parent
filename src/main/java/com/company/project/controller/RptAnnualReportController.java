package com.company.project.controller;

import com.company.project.common.aop.annotation.LogAnnotation;
import com.company.project.common.utils.DataResult;
import com.company.project.entity.RptAnnualReportEntity;
import com.company.project.entity.RptMatchingReportEntity;
import com.company.project.entity.RptStatisticsReportEntity;
import com.company.project.mapper.RptStatisticsReportMapper;
import com.company.project.service.RptAnnualReportService;
import com.company.project.service.RptStatisticsReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 工商报表
 */
@RestController
@Api(tags = "组织管理-工商报表")
@RequestMapping("/rpt")
@Slf4j
public class RptAnnualReportController {

    @Autowired
    private RptAnnualReportService rptAnnualReportService;

    @RequestMapping("/findAllAnnualReport")
    @ApiOperation(value = "分页获取工商报表列表接口")
    @RequiresPermissions("rpt:annualReport:findAll")
    @LogAnnotation(title = "工商报表", action = "分页获取工商报表列表")
    public DataResult pageInfo(@RequestBody RptAnnualReportEntity rptAnnualReportEntity) {

        return DataResult.success(rptAnnualReportService.pageInfo(rptAnnualReportEntity));
    }

    @RequestMapping("/annualReportUploadExcel")
    @ApiOperation(value = "Excel上传接口")
    @RequiresPermissions("rpt:annualReport:upload")
    public DataResult annualReportUploadExcel(MultipartFile file, String reportStatus) throws IOException {

        //获取Excel上传的数据的集合
        List<RptAnnualReportEntity> list = rptAnnualReportService.annualReportUploadExcel(file, Integer.parseInt(reportStatus));

        List<RptAnnualReportEntity> addList = new ArrayList<>();
        List<RptAnnualReportEntity> updateList = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            //获取Excel上传数据集合的社会信用代码
            String socialCreditCode = list.get(i).getSocialCreditCode();

            //通过社会信用代码查询数据库是否已存在该数据
            if (rptAnnualReportService.findSocialCreditCodeIsHave(socialCreditCode) == null) {
                //如果不存在,添加到addList集合
                addList.add(list.get(i));
            } else {
                //如果存在，再通过社会信用代码查询数据库中该条数据是否一致
                if (rptAnnualReportService.findIdenticalBySocialCreditCode(list.get(i)) == null) {
                    //如果不一致,添加到updateList集合
                    updateList.add(list.get(i));
                } else {
                    //如果一致，不执行任何操作
                }
            }
        }
        if (addList.size() > 0) {
            //如果addList不为空，执行添加
            rptAnnualReportService.addAnnualReport(addList);
        }
        if (updateList.size() > 0) {
            //如果updateList不为空，执行修改
            rptAnnualReportService.updateAnnualReport(updateList);
        }

        int result = 1;

        return DataResult.success(result);
    }


}
