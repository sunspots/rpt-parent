package com.company.project.controller;

import com.company.project.common.aop.annotation.LogAnnotation;
import com.company.project.common.utils.DataResult;
import com.company.project.entity.RptAnnualHistoryReportEntity;
import com.company.project.service.RptAnnualHistoryReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 历史报表-工商历史报表
 */
@RestController
@Api(tags = "组织管理-历史报表-工商历史报表")
@RequestMapping("/annualHistoryRpt")
@Slf4j
public class RptAnnualHistoryReportController {

    @Autowired
    RptAnnualHistoryReportService rptAnnualHistoryReportService;

    @RequestMapping("/findAllAnnualHistoryReport")
    @ApiOperation(value = "分页获取工商历史报表列表接口")
    @RequiresPermissions("rpt:annualHistoryReport:findAll")
    @LogAnnotation(title = "工商历史报表", action = "分页获取工商历史报表列表")
    public DataResult findAllAnnualHistoryReport(@RequestBody RptAnnualHistoryReportEntity rptAnnualHistoryReportEntity){

        return DataResult.success(rptAnnualHistoryReportService.findAllAnnualHistoryReport(rptAnnualHistoryReportEntity));

    }


}
