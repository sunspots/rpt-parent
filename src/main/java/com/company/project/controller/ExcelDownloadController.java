package com.company.project.controller;

import com.company.project.common.utils.DataResult;
import com.company.project.entity.*;
import com.company.project.mapper.*;
import com.company.project.service.ExcelDownloadService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Excel下载
 */
@RestController
@Api(tags = "Excel下载")
@RequestMapping("/download")
@Slf4j
public class ExcelDownloadController {
    @Autowired
    ExcelDownloadService excelDownloadService;
    @Autowired
    RptAnnualHistoryReportMapper rptAnnualHistoryReportMapper;
    @Autowired
    private RptApprovalHistoryReportMapper rptApprovalHistoryReportMapper;
    @Autowired
    private RptStatisticsReportMapper rptStatisticsReportMapper;
    @Autowired
    private RptMatchingReportMapper rptMatchingReportMapper;
    @Autowired
    private RptQingDiaoReportMapper rptQingDiaoReportMapper;
    @Autowired
    private RptTaxationHistoryReportMapper rptTaxationHistoryReportMapper;
    @Autowired
    private RptQingDiaoHistoryReportMapper rptQingDiaoHistoryReportMapper;

    @RequestMapping(value = "/matchingDownload", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "匹配报表下载")
    public void matchingDownload(HttpServletResponse response) throws IOException {
        String title = "matchingExcel";
        List<RptMatchingReportEntity> list = rptMatchingReportMapper.findAllMatchingReport(new RptMatchingReportEntity());
        excelDownloadService.matchingDownload(title, list, response);

    }

    @RequestMapping(value = "/matchingDownloadEmpty", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "匹配报表下载（含税务状态）")
    public void matchingDownloadEmpty(HttpServletResponse response) throws IOException {
        String title = "matchingExcelEmpty";
        List<RptQingdiaoReportEntity> list = rptQingDiaoReportMapper.findAllQingDiaoReport(new RptQingdiaoReportEntity());
        //List<RptMatchingReportEntity> list = rptMatchingReportMapper.findAllMatchingReport(new RptMatchingReportEntity());
        excelDownloadService.matchingDownloadEmpty(title, list, response);

    }


    @RequestMapping(value = "/statisticsReportExcel", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "统计报表-查询所有统计数据")
    public void statisticsReportExcel(HttpServletResponse response){
        String title = "statisticsReportExcel";
        List<RptStatisticsReportEntity> list = rptStatisticsReportMapper.findAllStatisticsReport();
        //List<RptStatisticsHistoryReportEntity> rptStatisticsHistoryReportEntity = excelDownloadService.findAllStatisticsReport();
        excelDownloadService.statisticsReportExcel(title, list, response);
    }

    @RequestMapping(value = "/statisticsReportExcelEmpty", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "统计报表-查询所有统计数据（包括清吊和空号）")
    public void statisticsReportExcelEmpty(HttpServletResponse response){
         String title = "statisticsReportExcel(haveEmpty)";
         List<RptStatisticsReportEntity> list = rptStatisticsReportMapper.findAllStatisticsReport();
         // List<RptStatisticsHistoryReportEntity> rptStatisticsHistoryReportEntity = excelDownloadService.findAllStatisticsReport();
         excelDownloadService.statisticsReportExcel1(title, list, response);
    }


    static List<RptAnnualHistoryReportEntity> rptAnnualHistoryReportEntityList;
    @RequestMapping(value = "/annualHistoryFileName", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "工商历史表（获取表名）")
    public DataResult annualHistoryFileName(@RequestBody RptAnnualHistoryReportEntity rptAnnualHistoryReportEntity){

        //rptApprovalHistoryReportEntityList = rptApprovalHistoryReportMapper.findAllApprovalHistoryReport(rptApprovalHistoryReportEntity);
        rptAnnualHistoryReportEntityList = rptAnnualHistoryReportMapper.findAllAnnualHistoryReport(rptAnnualHistoryReportEntity);
        String fileName = "annualHistory" + new Date().toString();
        return DataResult.success(fileName);

    }

    @RequestMapping(value = "/annualHistoryDownload", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "工商历史表下载")
    public void annualHistoryDownload(@RequestParam String fileName,HttpServletResponse response){

        excelDownloadService.annualHistoryDownload(fileName, rptAnnualHistoryReportEntityList, response);

    }


    static List<RptApprovalHistoryReportEntity> rptApprovalHistoryReportEntityList;
    @RequestMapping(value = "/rptApprovalHistoryFileName", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "核准历史表（获取表名）")
    public DataResult rptApprovalHistoryFileName(@RequestBody RptApprovalHistoryReportEntity rptApprovalHistoryReportEntity){

        rptApprovalHistoryReportEntityList = rptApprovalHistoryReportMapper.findAllApprovalHistoryReport(rptApprovalHistoryReportEntity);

        String fileName = "ApprovalHistoryHistory" + new Date().toString();
        return DataResult.success(fileName);

    }

    @RequestMapping(value = "/rptApprovalHistoryDownload", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "核准历史表下载")
    public void rptApprovalHistoryDownload(@RequestParam String fileName,HttpServletResponse response){

        excelDownloadService.rptApprovalHistoryDownload(fileName, rptApprovalHistoryReportEntityList, response);

    }


    static List<RptTaxationHistoryReportEntity> rptTaxationHistoryReportEntityList;
    @RequestMapping(value = "/taxationHistoryFileName", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "税务历史表（获取表名）")
    public DataResult taxationHistoryFileName(@RequestBody RptTaxationHistoryReportEntity rptTaxationHistoryReportEntity){


        rptTaxationHistoryReportEntityList = rptTaxationHistoryReportMapper.findAllTaxationHistoryReport(rptTaxationHistoryReportEntity);
        String fileName = "TaxationHistoryHistory" + new Date().toString();
        return DataResult.success(fileName);

    }

    @RequestMapping(value = "/taxationHistoryDownload", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "税务历史表下载")
    public void taxationHistoryDownload(@RequestParam String fileName,HttpServletResponse response){

        excelDownloadService.rptTaxationHistoryDownload(fileName, rptTaxationHistoryReportEntityList, response);

    }


    static List<RptMatchingHistoryReportEntity> rptMatchingHistoryReportEntityList;
    @RequestMapping(value = "/matchingHistoryReportFileName", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "匹配历史表（获取表名）")
    public DataResult matchingHistoryReportFileName(@RequestBody RptMatchingHistoryReportEntity rptMatchingHistoryReportEntity){

        rptMatchingHistoryReportEntityList = rptMatchingReportMapper.pageHistoryInfo(rptMatchingHistoryReportEntity);
        String fileName = "matchingHistory" + new Date().toString();
        return DataResult.success(fileName);

    }

    @RequestMapping(value = "/matchingHistoryDownload", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "匹配历史表下载")
    public void matchingHistoryDownload(@RequestParam String fileName,HttpServletResponse response){

        excelDownloadService.matchingHistoryDownload(fileName, rptMatchingHistoryReportEntityList, response);

    }


    static List<RptQingdiaoHistoryReportEntity> rptQingdiaoHistoryReportEntityList;
    @RequestMapping(value = "/qingDiaoHistoryReportFileName", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "清吊历史表（获取表名）")
    public DataResult qingDiaoHistoryReportFileName(@RequestBody RptQingdiaoHistoryReportEntity rptQingdiaoHistoryReportEntity){

        rptQingdiaoHistoryReportEntityList = rptQingDiaoHistoryReportMapper.findAllQingDiaoHistoryReport(rptQingdiaoHistoryReportEntity);
        String fileName = "qingDiaoHistory" + new Date().toString();
        return DataResult.success(fileName);

    }

    @RequestMapping(value = "/qingDiaoHistoryDownload", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "清吊历史表下载")
    public void qingDiaoHistoryDownload(@RequestParam String fileName,HttpServletResponse response){

        excelDownloadService.qingDiaoHistoryDownload(fileName, rptQingdiaoHistoryReportEntityList, response);

    }
}
