package com.company.project.controller;


import com.company.project.entity.RptRobotProblemEntity;
import com.company.project.service.RptRobotProblemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class RptRobotProblemController {
    @Autowired
    private RptRobotProblemService problemService;

    @PostMapping("/addProblem")
    public Map<String,Object> addProblem(@RequestBody RptRobotProblemEntity problem){
        Map<String, Object> map = new HashMap<>();
        Integer integer = problemService.addProblem(problem);
        map.put("data", integer);
        return map;
    }

    @PostMapping("/updateProblem")
    public Map<String, Object> updateProblem(@RequestBody RptRobotProblemEntity problem) {
        Map<String, Object> map = new HashMap<>();
        Integer integer = problemService.updateProblem(problem);
        map.put("data", integer);
        return map;
    }

    @GetMapping("/getProblemById")
    public Map<String,Object> getProblemById(@RequestParam Integer id){
        Map<String, Object> map = new HashMap<>();
        RptRobotProblemEntity problemById = problemService.getProblemById(id);
        map.put("data", problemById);
        return map;
    }

    @GetMapping("/getAllProblem")
    public Map<String,Object> getAllProblem(){
        Map<String, Object> map = new HashMap<>();
        List<RptRobotProblemEntity> allProblem = problemService.getAllProblem();
        map.put("data", allProblem);
        return map;
    }
}
