package com.company.project.controller;

import com.company.project.common.aop.annotation.LogAnnotation;
import com.company.project.common.utils.DataResult;
import com.company.project.entity.RptTaxationReportEntity;
import com.company.project.service.RptTaxationReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 税务报表
 */
@RestController
@Api(tags = "组织模块-税务报表")
@RequestMapping("/taxationRpt")
@Slf4j
public class RptTaxationReportController {

    @Autowired
    private RptTaxationReportService rptTaxationReportService;

    @RequestMapping("/findAllTaxationReport")
    @ApiOperation(value = "分页获取税务报表列表接口")
    @RequiresPermissions("rpt:taxationReport:findAll")
    @LogAnnotation(title = "税务报表", action = "分页获取税务报表列表")
    public DataResult findAllTaxationReport(@RequestBody RptTaxationReportEntity rptTaxationReportEntity){

        return DataResult.success(rptTaxationReportService.findAllTaxationReport(rptTaxationReportEntity));
    }

    /**
     * Excel上传
     * @param file
     * @param reportStatus
     * @return
     * @throws IOException
     */
    @PostMapping("/taxationReportUploadExcel")
    @ApiOperation(value = "Excel上传接口")
    @RequiresPermissions("rpt:taxationReport:upload")
    public DataResult taxationReportUploadExcel(MultipartFile file, Integer reportStatus) throws IOException {

        //获取Excel上传的数据的集合
        List<RptTaxationReportEntity> list = rptTaxationReportService.taxationReportUploadExcel(file,reportStatus);

        List<RptTaxationReportEntity> addList = new ArrayList<>();
        List<RptTaxationReportEntity> updateList = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            //获取Excel上传数据集合的社会信用代码
            String socialCreditCode = list.get(i).getSocialCreditCode();

            //通过社会信用代码查询数据库是否已存在该数据
            if (rptTaxationReportService.findSocialCreditCodeIsHave(socialCreditCode) == null){
                //如果不存在,添加到addList集合
                addList.add(list.get(i));
            }else {
                //如果存在，再通过社会信用代码查询数据库中该条数据是否一致
                if (rptTaxationReportService.findIdenticalBySocialCreditCode(list.get(i)) == null){
                    //如果不一致,添加到updateList集合
                    updateList.add(list.get(i));
                }else {
                    //如果一致，不执行任何操作
                }
            }
        }
        if (addList.size() > 0){
            //如果addList不为空，执行添加
            rptTaxationReportService.addTaxationReport(addList);
        }
        if (updateList.size() > 0){
            //如果updateList不为空，执行修改
            rptTaxationReportService.updateTaxationReport(updateList);
        }

        int result = 1;

        return DataResult.success(result);
    }


}
