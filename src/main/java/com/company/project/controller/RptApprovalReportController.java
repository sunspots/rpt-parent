package com.company.project.controller;

import com.company.project.common.aop.annotation.LogAnnotation;
import com.company.project.common.utils.DataResult;
import com.company.project.entity.RptApprovalReportEntity;
import com.company.project.service.RptApprovalReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 核准报表
 */
@RestController
@Api(tags = "组织管理-核准报表")
@RequestMapping("/approvalRpt")
@Slf4j
public class RptApprovalReportController {

    @Autowired
    private RptApprovalReportService rptApprovalReportService;

    @RequestMapping("/findAllApprovalReport")
    @ApiOperation(value = "分页获取核准报表列表接口")
    @RequiresPermissions("rpt:approvalReport:findAll")
    @LogAnnotation(title = "核准报表", action = "分页获取核准报表列表")
    public DataResult findAllApprovalReport(@RequestBody RptApprovalReportEntity rptApprovalReportEntity){

        return DataResult.success(rptApprovalReportService.findAllApprovalReport(rptApprovalReportEntity));
    }

    @RequestMapping("/addApprovalReport")
    @ApiOperation(value = "添加核准报表接口")
    @RequiresPermissions("rpt:approvalReport:add")
    @LogAnnotation(title = "核准报表", action = "添加核准报表")
    public DataResult addApprovalReport(@RequestBody RptApprovalReportEntity rptApprovalReportEntity){

        //获取当前时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String crateTime = simpleDateFormat.format(new Date());
        rptApprovalReportEntity.setCreateTime(crateTime);

        List<RptApprovalReportEntity> rptApprovalReportEntityList = new ArrayList<>();
        rptApprovalReportEntityList.add(rptApprovalReportEntity);

        int result = rptApprovalReportService.addApprovalReport(rptApprovalReportEntityList);

        return DataResult.success(result);
    }

    @RequestMapping("/updateApprovalReport")
    @ApiOperation(value = "修改核准报表接口")
    @RequiresPermissions("rpt:approvalReport:update")
    @LogAnnotation(title = "核准报表", action = "修改核准报表")
    public DataResult updateApprovalReport(@RequestBody RptApprovalReportEntity rptApprovalReportEntity){

        //获取当前时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String crateTime = simpleDateFormat.format(new Date());
        rptApprovalReportEntity.setCreateTime(crateTime);

        int result = rptApprovalReportService.updateApprovalReport(rptApprovalReportEntity);

        return DataResult.success(result);
    }

    @RequestMapping("/findByIdApprovalReport")
    @ApiOperation(value = "通过ID查询核准报表接口")
    @RequiresPermissions("rpt:approvalReport:findById")
    @LogAnnotation(title = "核准报表", action = "通过ID查询核准报表")
    public DataResult findByIdApprovalReport(@RequestParam(value = "id") Integer approvalReportId){

        return DataResult.success(rptApprovalReportService.findByIdApprovalReport(approvalReportId));
    }

    @RequestMapping("/delApprovalReport")
    @ApiOperation(value = "删除核准报表接口")
    @RequiresPermissions("rpt:approvalReport:del")
    @LogAnnotation(title = "核准报表", action = "删除核准报表")
    public DataResult delApprovalReport(@RequestBody @ApiParam(value = "id集合") List<Integer> ids){

        int result = rptApprovalReportService.delApprovalReport(ids);

        return DataResult.success(result);
    }

    @RequestMapping("/approvalReportUploadExcel")
    @ApiOperation(value = "Excel上传接口")
    @RequiresPermissions("rpt:approvalReport:upload")
    public DataResult approvalReportUploadExcel(MultipartFile file, Integer uploadType) throws IOException{

        //获取Excel上传的数据的集合
        List<RptApprovalReportEntity> list = rptApprovalReportService.approvalReportUploadExcel(file);

        int result = 0;

        if (uploadType == 1){
            //全量上传
            //1.查询出核准报表所有数据
            List<RptApprovalReportEntity> historyList = rptApprovalReportService.findAll();
            //2.将数据添加进历史报表
            rptApprovalReportService.addApprovalHistoryReport(historyList);
            //3.删除核准报表所有数据
            rptApprovalReportService.delAllApprovalReport();
            //4.Excel上传的数据执行添加
            rptApprovalReportService.addApprovalReport(list);
            result = 1;
        }else {
            //增量上传
            //直接执行添加
            rptApprovalReportService.addApprovalReport(list);
            result = 1;
        }

        return DataResult.success(result);
    }


}
