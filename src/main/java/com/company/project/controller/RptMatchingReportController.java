package com.company.project.controller;

import com.company.project.common.aop.annotation.LogAnnotation;
import com.company.project.common.utils.DataResult;
import com.company.project.entity.RptAnnualReportEntity;
import com.company.project.entity.RptApprovalReportEntity;
import com.company.project.entity.RptMatchingHistoryReportEntity;
import com.company.project.entity.RptMatchingReportEntity;
import com.company.project.service.RptMatchingReportService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 匹配报表
 */
@RestController
@Api(tags = "组织管理-匹配报表")
@RequestMapping("/matchingRpt")
@Slf4j
public class RptMatchingReportController {

    @Autowired
    private RptMatchingReportService rptMatchingReportService;

    @PostMapping("/findAllMatchingReport")
    @ApiOperation(value = "分页获取匹配报表列表接口")
    @RequiresPermissions("rpt:matchingReport:findAll")
    @LogAnnotation(title = "匹配报表", action = "分页获取匹配报表列表")
    public DataResult findAllMatchingReport(@RequestBody RptMatchingReportEntity rptMatchingReportEntity){
        PageInfo<RptMatchingReportEntity> pageInfo = rptMatchingReportService.pageInfo(rptMatchingReportEntity);
        return DataResult.success(pageInfo);
    }

    @GetMapping("/clickMatching")
    @ApiOperation(value = "把工商报表与核准报表进行匹配接口")
    @RequiresPermissions("rpt:matchingReport:matching")
    @LogAnnotation(title = "匹配报表", action = "把工商报表与核准报表进行匹配")
    public DataResult clickMatching(){
        Integer result1 = rptMatchingReportService.findMatchingReportByDominationAndAddress();
        Integer result2 = rptMatchingReportService.findNotMatchingReportByDominationAndAddress();
        if (result1 == 0) {
            return DataResult.fail("无匹配成功数据 !!!");
        }
        return DataResult.success("匹配完成 !!!");
    }

    @PostMapping("/changeMatchingStatus")
    @ApiOperation(value = "手动修改匹配状态，将失败修改为成功接口")
    @RequiresPermissions("rpt:matchingReport:update")
    @LogAnnotation(title = "匹配报表", action = "手动修改匹配状态，将失败修改为成功")
    public DataResult changeMatchingStatus(@RequestBody Integer id){
        RptMatchingReportEntity matching = rptMatchingReportService.findMatchingReportById(id);
        if (matching == null) {
            return DataResult.fail("不存在该条信息 !!!");
        }
        if (matching.getConfirmStatus() == 1) {
            return DataResult.fail("已确认该条信息 !!!");
        }
        Integer result = rptMatchingReportService.addMatchingFailToSuccess(matching);
        if (result == 0) {
            return DataResult.fail("2");
        }
        return DataResult.success("1");
    }

    @PostMapping("/findHighMatchingAddress")
    @ApiOperation(value = "查询地址匹配度接口")
    @RequiresPermissions("rpt:matchingReport:matchingValue")
    @LogAnnotation(title = "匹配报表", action = "查询地址匹配度")
    public DataResult findHighMatchingAddress(@RequestBody Integer id){
        List<RptMatchingReportEntity> list = rptMatchingReportService.findHighMatchingAddress(id);
        if (list.size() == 0) {
            return DataResult.success("2");
        }
        return DataResult.success(list);
    }

    @PostMapping("/findAllMatchingHistoryReport")
    @ApiOperation(value = "匹配历史表模糊查询接口")
    @RequiresPermissions("rpt:matchingHistoryReport:findAll")
    @LogAnnotation(title = "匹配历史报表", action = "查询地址匹配度")
    public DataResult findMatchingHistoryReport(@RequestBody RptMatchingHistoryReportEntity rptMatchingHistoryReportEntity){
        PageInfo<RptMatchingHistoryReportEntity> pageInfo = rptMatchingReportService.pageHistoryInfo(rptMatchingHistoryReportEntity);
        return DataResult.success(pageInfo);
    }



}
