package com.company.project.controller;

import com.company.project.common.aop.annotation.LogAnnotation;
import com.company.project.common.utils.DataResult;
import com.company.project.entity.RptApprovalHistoryReportEntity;
import com.company.project.service.RptApprovalHistoryReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 历史报表-核准历史报表
 */
@RestController
@Api(tags = "组织管理-历史报表-核准历史报表")
@RequestMapping("/approvalHistoryRpt")
@Slf4j
public class RptApprovalHistoryReportController {

    @Autowired
    private RptApprovalHistoryReportService rptApprovalHistoryReportService;

    @RequestMapping("/findAllApprovalHistoryReport")
    @ApiOperation(value = "分页获取核准历史报表列表接口")
    @RequiresPermissions("rpt:approvalHistoryReport:findAll")
    @LogAnnotation(title = "核准历史报表", action = "分页获取核准历史报表列表")
    public DataResult findAllApprovalHistoryReport(@RequestBody RptApprovalHistoryReportEntity rptApprovalHistoryReportEntity){

        return DataResult.success(rptApprovalHistoryReportService.findAllApprovalHistoryReport(rptApprovalHistoryReportEntity));
    }





}
