package com.company.project.controller;

import com.company.project.common.aop.annotation.LogAnnotation;
import com.company.project.common.utils.DataResult;
import com.company.project.entity.RptStatisticsHistoryReportEntity;
import com.company.project.entity.RptStatisticsReportEntity;
import com.company.project.service.RptStatisticsReportService;
import com.github.pagehelper.PageInfo;
import com.sun.xml.internal.stream.StaxErrorReporter;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 统计报表
 */
@RestController
@Api(tags = "组织管理-统计报表")
@RequestMapping("/statisticsRpt")
@Slf4j
public class RptStatisticsReportController {

    @Autowired
    private RptStatisticsReportService rptStatisticsReportService;

    @PostMapping("/statisticsReport")
    @ApiOperation(value = "点击统计接口")
    @RequiresPermissions("rpt:statisticsReport:statisticsReport")
    @LogAnnotation(title = "统计报表", action = "点击统计")
    public Map<String,Object> statisticsReport(){
        Map<String, Object> map = new HashMap<>();
        Map<String, Object> result = rptStatisticsReportService.findAddressAnnualReportByAddress();
        Map<String, Object> result2 = rptStatisticsReportService.findEmptyAndClearNumberReportByAddress();
        map.put("code", 0);
        return map;
    }

    @PostMapping("/findAllStatisticsReport")
    @ApiOperation(value = "查询所有统计数据（包含清吊和空号）")
    @RequiresPermissions("rpt:statisticsReport:findAll")
    @LogAnnotation(title = "统计报表", action = "查询所有统计数据（包括清吊和空号）")
    public Map<String,Object> findAllStatisticsReport(){
        Map<String, Object> map = new HashMap<>();
        Map<String, Object> result = rptStatisticsReportService.findAllStatisticsReport();
        map.put("code", 0);
        map.put("data", result);
        return map;
    }

    @PostMapping("/findAllStatisticsHistory")
    @ApiOperation(value = "历史表模糊查询接口")
    @RequiresPermissions("rpt:statisticsHistoryReport:findAll")
    @LogAnnotation(title = "统计历史报表", action = "历史表模糊查询")
    public DataResult findStatisticsHistory(@RequestBody RptStatisticsHistoryReportEntity rptStatisticsHistoryReportEntity){
        PageInfo<RptStatisticsHistoryReportEntity> list = rptStatisticsReportService.findStatisticsHistory(rptStatisticsHistoryReportEntity);
        return DataResult.success(list);
    }

    @PostMapping("/findAllStatisticsHistory1")
    @ApiOperation(value = "历史表模糊查询接口(含清吊和空号)")
    @RequiresPermissions("rpt:statisticsHistoryReport1:findAll")
    @LogAnnotation(title = "统计历史报表(含清吊和空号)", action = "历史表模糊查询(含清吊和空号)")
    public DataResult findStatisticsHistory1(@RequestBody RptStatisticsHistoryReportEntity rptStatisticsHistoryReportEntity){
        PageInfo<RptStatisticsHistoryReportEntity> list = rptStatisticsReportService.findStatisticsHistory1(rptStatisticsHistoryReportEntity);
        return DataResult.success(list);
    }



}
