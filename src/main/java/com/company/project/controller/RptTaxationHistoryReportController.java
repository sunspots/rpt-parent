package com.company.project.controller;

import com.company.project.common.aop.annotation.LogAnnotation;
import com.company.project.common.utils.DataResult;
import com.company.project.entity.RptTaxationHistoryReportEntity;
import com.company.project.service.RptTaxationHistoryReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 税务历史报表
 */
@RestController
@Api(tags = "组织模块-历史报表-税务历史报表")
@RequestMapping("/taxationHistoryRpt")
@Slf4j
public class RptTaxationHistoryReportController {

    @Autowired
    private RptTaxationHistoryReportService rptTaxationHistoryReportService;

    @RequestMapping("/findAllTaxationHistoryReport")
    @ApiOperation(value = "分页获取税务历史报表列表接口")
    @RequiresPermissions("rpt:taxationHistoryReport:findAll")
    @LogAnnotation(title = "税务历史报表", action = "分页获取税务历史报表列表")
    public DataResult findAllTaxationHistoryReport(@RequestBody RptTaxationHistoryReportEntity rptTaxationHistoryReportEntity){

        return DataResult.success(rptTaxationHistoryReportService.findAllTaxationHistoryReport(rptTaxationHistoryReportEntity));

    }


}
