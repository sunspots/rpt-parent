package com.company.project.controller;

import com.company.project.common.aop.annotation.LogAnnotation;
import com.company.project.common.utils.DataResult;
import com.company.project.entity.RptQingdiaoReportEntity;
import com.company.project.service.RptQingDiaoReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;

/**
 * 清吊报表
 */
@RestController
@Api(tags = "组织模块-匹配报表-清吊信息")
@RequestMapping("/qingdiaoRpt")
@Slf4j
public class RptQingDiaoReportController {

    @Autowired
    private RptQingDiaoReportService rptQingDiaoReportService;

    @RequestMapping("/findAllQingDiaoReport")
    @ApiOperation(value = "分页获取清吊报表列表接口")
    @RequiresPermissions("rpt:qingdiaoReport:findAll")
    @LogAnnotation(title = "清吊报表", action = "分页获取清吊报表列表")
    public DataResult findAllQingDiaoReport(@RequestBody RptQingdiaoReportEntity rptQingdiaoReportEntity){

        return DataResult.success(rptQingDiaoReportService.findAllQingDiaoReport(rptQingdiaoReportEntity));
    }

    @RequestMapping("/triggerQingDiaoForTaxationReport")
    @ApiOperation(value = "点击触发清吊事件接口")
    @RequiresPermissions("rpt:qingdiaoReport:trigger")
    @LogAnnotation(title = "清吊报表", action = "点击触发清吊事件")
    public DataResult triggerQingDiaoForTaxationReport() throws ParseException {

        Integer result = rptQingDiaoReportService.triggerQingDiaoForTaxationReport();

        return DataResult.success(result);
    }

    @RequestMapping("/addQingDiaoReportForEmpty")
    @ApiOperation(value = "添加状态为空号的清吊信息接口")
    @RequiresPermissions("rpt:qingdiaoReport:add")
    @LogAnnotation(title = "清吊报表", action = "添加状态为空号的清吊信息")
    public DataResult addQingDiaoReportForEmpty(@RequestBody RptQingdiaoReportEntity rptQingdiaoReportEntity){

        Integer result = rptQingDiaoReportService.addQingDiaoReportForEmpty(rptQingdiaoReportEntity);

        return DataResult.success(result);
    }


}
