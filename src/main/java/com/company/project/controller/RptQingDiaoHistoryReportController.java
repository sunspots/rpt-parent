package com.company.project.controller;

import com.company.project.common.aop.annotation.LogAnnotation;
import com.company.project.common.utils.DataResult;
import com.company.project.entity.RptQingdiaoHistoryReportEntity;
import com.company.project.service.RptQingDiaoHistoryReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 清吊历史报表
 */
@RestController
@Api(tags = "组织模块-历史报表-清吊历史报表")
@RequestMapping("/qingdiaoHistoryRpt")
@Slf4j
public class RptQingDiaoHistoryReportController {

    @Autowired
    private RptQingDiaoHistoryReportService rptQingDiaoHistoryReportService;

    @RequestMapping("/findAllQingDiaoHistoryReport")
    @ApiOperation(value = "分页获取清吊历史报表列表接口")
    @RequiresPermissions("rpt:qingdiaoHistoryReport:findAll")
    @LogAnnotation(title = "清吊历史报表", action = "分页获取清吊历史报表列表")
    public DataResult findAllQingDiaoReport(@RequestBody RptQingdiaoHistoryReportEntity rptQingdiaoHistoryReportEntity){

        return DataResult.success(rptQingDiaoHistoryReportService.findAllQingDiaoHistoryReport(rptQingdiaoHistoryReportEntity));
    }



}
