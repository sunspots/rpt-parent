package com.company.project.mapper;

import com.company.project.entity.RptApprovalReportEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RptApprovalReportMapper {

    //查询核准报表
    List<RptApprovalReportEntity> findAllApprovalReport(RptApprovalReportEntity rptApprovalReportEntity);

    //查询核准报表所有数据
    List<RptApprovalReportEntity> findAll();

    //查询所有核准报表条数
    Integer findCountApprovalReport();

    //核准报表添加
    Integer addApprovalReport(List<RptApprovalReportEntity> rptApprovalReportEntityList);

    //核准报表修改
    Integer updateApprovalReport(RptApprovalReportEntity rptApprovalReportEntity);

    //核准报表查询通过id
    RptApprovalReportEntity findByIdApprovalReport(Integer approvalReportId);

    //核准报表删除
    Integer delApprovalReport(List<Integer> ids);

    //删除核准报表所有数据
    Integer delAllApprovalReport();

}
