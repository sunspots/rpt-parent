package com.company.project.mapper;

import com.company.project.entity.RptApprovalHistoryReportEntity;
import com.company.project.entity.RptApprovalReportEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RptApprovalHistoryReportMapper {

    //查询核准历史报表
    List<RptApprovalHistoryReportEntity> findAllApprovalHistoryReport(RptApprovalHistoryReportEntity rptApprovalHistoryReportEntity);

    //查询所有核准历史报表条数
    Integer findCountApprovalHistoryReport();

    //添加核准历史报表
    Integer addApprovalHistoryReport(List<RptApprovalReportEntity> rptApprovalReportEntityList);

}
