package com.company.project.mapper;

import com.company.project.entity.RptTaxationReportEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RptTaxationReportMapper {

    //查询税务报表
    List<RptTaxationReportEntity> findAllTaxationReport(RptTaxationReportEntity rptTaxationReportEntity);

    //查询税务报表所有数据
    List<RptTaxationReportEntity> findAll();

    //通过社会信用代码查询在数据库是否已存在
    RptTaxationReportEntity findSocialCreditCodeIsHave(String socialCreditCode);

    //通过社会信用代码查询数据库中该条数据是否一致
    RptTaxationReportEntity findIdenticalBySocialCreditCode(RptTaxationReportEntity rptTaxationReportEntity);

    //添加税务报表
    Integer addTaxationReport(List<RptTaxationReportEntity> rptTaxationReportEntityList);

    //修改税务报表
    Integer updateTaxationReport(RptTaxationReportEntity rptTaxationReportEntity);

    //删除税务报表
    Integer delTaxationReport();


}
