package com.company.project.mapper;

import com.company.project.entity.RptStatisticsHistoryReportEntity;
import com.company.project.entity.RptStatisticsReportEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Repository
public interface RptStatisticsReportMapper {

    Integer addStatisticsReport(RptStatisticsReportEntity rptStatisticsReportEntity);

    Integer updateStatisticsReport(RptStatisticsReportEntity rptStatisticsReportEntity);

    RptStatisticsReportEntity findStatisticsReportByDateAndStatus(RptStatisticsReportEntity rptStatisticsReportEntity);

    Integer deleteAllStatisticsReport();

    List<RptStatisticsReportEntity> findAllStatisticsReport();

    Integer autoSetMatchingReportToHistory(List<RptStatisticsReportEntity> list);

    List<RptStatisticsHistoryReportEntity> findStatisticsHistory(RptStatisticsHistoryReportEntity rptStatisticsHistoryReportEntity);

    List<RptStatisticsHistoryReportEntity> findStatisticsHistory1(RptStatisticsHistoryReportEntity rptStatisticsHistoryReportEntity);
}
