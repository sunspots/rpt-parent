package com.company.project.mapper;

import com.company.project.entity.RptTaxationHistoryReportEntity;
import com.company.project.entity.RptTaxationReportEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RptTaxationHistoryReportMapper {

    //查询税务历史报表
    List<RptTaxationHistoryReportEntity> findAllTaxationHistoryReport(RptTaxationHistoryReportEntity rptTaxationHistoryReportEntity);

    //添加到税务历史表
    Integer addTaxationHistoryReport(List<RptTaxationReportEntity>  rptTaxationReportEntityList);


}
