package com.company.project.mapper;

import com.company.project.entity.RptAnnualReportEntity;
import com.company.project.entity.RptQingdiaoReportEntity;
import com.company.project.entity.RptTaxationReportEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RptQingDiaoReportMapper {

    //查询清吊报表
    List<RptQingdiaoReportEntity> findAllQingDiaoReport(RptQingdiaoReportEntity rptQingdiaoReportEntity);

    //查询清吊报表所有数据
    List<RptQingdiaoReportEntity> findAll();

    //通过社会信用代码查询在数据库是否已存在
    RptQingdiaoReportEntity findSocialCreditCodeIsHave(String socialCreditCode);

    //通过社会信用代码查询数据库中该条数据是否一致
    RptQingdiaoReportEntity findIdenticalBySocialCreditCode(RptQingdiaoReportEntity rptQingdiaoReportEntity);

    //添加清吊报表
    Integer addQingDiaoReport(List<RptQingdiaoReportEntity> rptQingdiaoReportEntityList);

    //修改清吊报表
    Integer updateQingDiaoReport(RptQingdiaoReportEntity rptQingdiaoReportEntity);

    //删除清吊报表
    Integer delQingDiaoReport();

    //查询税务报表税务状态为黑名单的数据
    List<RptTaxationReportEntity> findTaxationReportForThree();

    //查询税务报表以社会信用代码为标识与工商报表匹配出税务状态为税务异常,上报状态为未上报的税务数据
    List<RptTaxationReportEntity> findTaxationReportForTwo();

    //查询工商报表以社会信用代码为标识与税务报表匹配出税务状态为税务异常,上报状态为未上报的工商数据
    List<RptAnnualReportEntity> findAnnualReportForTwo();

    //添加状态为空号的清吊信息
    Integer addQingDiaoReportForEmpty(RptQingdiaoReportEntity rptQingdiaoReportEntity);



}
