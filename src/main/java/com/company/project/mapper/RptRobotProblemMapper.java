package com.company.project.mapper;

import com.company.project.entity.RptRobotProblemEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RptRobotProblemMapper {

    Integer addProblem(RptRobotProblemEntity problem);

    Integer updateProblem(RptRobotProblemEntity problem);

    RptRobotProblemEntity getProblemById(Integer id);

    List<RptRobotProblemEntity> getAllProblem();

    List<RptRobotProblemEntity> getProblem(RptRobotProblemEntity problem);


}
