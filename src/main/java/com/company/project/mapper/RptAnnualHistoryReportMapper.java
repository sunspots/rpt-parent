package com.company.project.mapper;

import com.company.project.entity.RptAnnualHistoryReportEntity;
import com.company.project.entity.RptAnnualReportEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RptAnnualHistoryReportMapper {

    //查询工商历史报表
    List<RptAnnualHistoryReportEntity> findAllAnnualHistoryReport(RptAnnualHistoryReportEntity rptAnnualHistoryReportEntity);

    //添加工商历史报表
    Integer addAnnualHistoryReport(List<RptAnnualReportEntity> rptAnnualReportEntityList);

}
