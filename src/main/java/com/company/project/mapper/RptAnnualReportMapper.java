package com.company.project.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.company.project.entity.RptAnnualReportEntity;
import com.company.project.entity.RptStatisticsReportEntity;
import com.company.project.entity.RptTaxationReportEntity;
import com.company.project.service.RptTaxationReportService;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RptAnnualReportMapper extends BaseMapper<RptAnnualReportEntity> {

    //查询工商报表
    List<RptAnnualReportEntity> findAllAnnualReport(RptAnnualReportEntity rptAnnualReportEntity);

    //查询工商报表所有数据
    List<RptAnnualReportEntity> findAll();

    //通过社会信用代码查询数据库是否已存在该数据
    RptAnnualReportEntity findSocialCreditCodeIsHave(String socialCreditCode);

    //通过社会信用代码查询数据库中该条数据是否一致
    RptAnnualReportEntity findIdenticalBySocialCreditCode(RptAnnualReportEntity rptAnnualReportEntity);

    //添加工商报表
    Integer addAnnualReport(List<RptAnnualReportEntity> rptAnnualReportEntityList);

    //修改工商报表
    Integer updateAnnualReport(RptAnnualReportEntity rptAnnualReportEntity);

    //删除工商报表
    Integer delAnnualReport();

    // 通过街道地址查询所有街道的年报数量
    List<RptAnnualReportEntity> findAddressAnnualReportByAddress(RptStatisticsReportEntity rptStatisticsReportEntity);

    // 获得该街道上报的企业数
    List<RptAnnualReportEntity> findAddressAnnualReportByReportStatus(RptStatisticsReportEntity rptStatisticsReportEntity);

    // 获得街道名称
    List<String> findAddressName();

    // 通过街道地址查询所有街道的空号数量
    List<RptTaxationReportEntity> findEmptyNumberByAddress(RptTaxationReportEntity rptTaxationReportEntity);

    // 通过街道地址查询所有街道的清吊数量
    List<RptTaxationReportEntity> findClearNumberByAddress(RptTaxationReportEntity rptTaxationReportEntity);

}
