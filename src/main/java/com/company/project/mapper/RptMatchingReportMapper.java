package com.company.project.mapper;

import com.company.project.entity.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Repository
public interface RptMatchingReportMapper {

    //添加匹配报表
    Integer addMatchingReport(RptMatchingReportEntity rptMatchingReportEntity);

    //更新匹配报表
    Integer updateMatchingReport(RptMatchingReportEntity rptMatchingReportEntity);

    // 批量添加匹配数据
    Integer addBatchMatchingReport(List<RptMatchingReportEntity> list);

    // 批量更新匹配数据
    Integer updateBatchMatchingReport(List<RptMatchingReportEntity> list);

    // 删除当天匹配数据，只在存入历史表的时候执行
    Integer deleteMatchingReport();

    // 根据id查询某条匹配信息
    RptMatchingReportEntity findMatchingReportById(Integer id);

    //查询社会信用代码是否存在
    RptMatchingReportEntity findOneMatchingReportBySocialCreditCode(@RequestParam("code") String code, @RequestParam("date") String date);

    //查询管辖机关和地址是否匹配，匹配成功的企业
    List<RptAnnualReportEntity> findMatchingReportByDominationAndAddress();

    //查询管辖机关和地址是否匹配，未匹配成功的企业
    List<RptAnnualReportEntity> findNotMatchingReportByDominationAndAddress();

    //查询匹配报表
    List<RptMatchingReportEntity> findAllMatchingReport(RptMatchingReportEntity rptMatchingReportEntity);

    // 手工将匹配失败的信息改成匹配成功的数据添加到数据库中
    Integer addMatchingFailToSuccess(RptMatchingReportEntity rptMatchingReportEntity);

    // 添加当天的历史数据
    Integer addBatchMatchingHistoryReport(List<RptMatchingReportEntity> list);

    // 根据匹配失败的数据来查找确认表中是否有该数据
    RptMatchingConfirmReportEntity findMatchingConfirmByMatchingReport(RptAnnualReportEntity rptAnnualReportEntity );

    // 在管辖机关相同的情况下模糊查询地址
    List<String> findHighMatchingAddress(@RequestParam("domination")String domination,@RequestParam("street")String street,@RequestParam("address")String address);

    // 查询匹配历史表
    List<RptMatchingHistoryReportEntity> pageHistoryInfo(RptMatchingHistoryReportEntity rptMatchingHistoryReportEntity);


    // 通过街道地址查询所有街道的匹配年报数量
    List<RptMatchingReportEntity> findAddressMatchingReportByAddress(RptStatisticsReportEntity rptStatisticsReportEntity);

    // 获得该街道上报的企业数
    List<RptMatchingReportEntity> findAddressMatchingReportByReportStatus(RptStatisticsReportEntity rptStatisticsReportEntity);

    // 获得匹配空号数量
    List<RptTaxationReportEntity> findEmptyNumberByAddress(RptTaxationReportEntity rptTaxationReportEntity);

    // 获得匹配清吊数量
    List<RptTaxationReportEntity> findClearNumberByAddress(RptTaxationReportEntity rptTaxationReportEntity);


}
