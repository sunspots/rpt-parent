package com.company.project.mapper;

import com.company.project.entity.RptQingdiaoHistoryReportEntity;
import com.company.project.entity.RptQingdiaoReportEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RptQingDiaoHistoryReportMapper {

    //查询清吊历史报表
    List<RptQingdiaoHistoryReportEntity> findAllQingDiaoHistoryReport(RptQingdiaoHistoryReportEntity rptQingdiaoHistoryReportEntity);

    //添加清吊历史报表
    Integer addQingDiaoHistoryReport(List<RptQingdiaoReportEntity> rptQingdiaoReportEntityList);

}
