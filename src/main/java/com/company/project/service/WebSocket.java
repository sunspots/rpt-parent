package com.company.project.service;

import com.alibaba.fastjson.JSON;
import com.company.project.common.config.ApplicationHelper;
import com.company.project.entity.RptRobotProblemEntity;
import com.company.project.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArraySet;

@Component
@ServerEndpoint("/webSocket")
@Slf4j
public class WebSocket {

    private RptRobotProblemService problemService = (RptRobotProblemService) ApplicationHelper.getBean("rptRobotProblemService");

    private Session session;

    private static CopyOnWriteArraySet<WebSocket> webSocketSet = new CopyOnWriteArraySet<>();

    @OnOpen
    public void onOpen(Session session){
        this.session = session;
//        System.out.println(this);
        webSocketSet.add(this);
        log.info("【websocket消息】有新的连接，总数:{}" , webSocketSet.size());
    }

    @OnClose
    public void onClose(){
        webSocketSet.remove(this);

        log.info("【websocket消息】连接断开，总数:{}" , webSocketSet.size());
    }

    @OnMessage
    public void onMessage(Session session,String message){
        log.info("【websocket消息】收到客户端发来的消息:{}",message);
        /**
         * todo
         * 1. 接收前台客户发送来的消息
         * 2. 将发送来的问题与数据库的问题进行匹配
         * 3. 返回数据库的问题答案
         * 4. 推送给前端，显示在页面上
         */
        String result = "";
        // 1. 接收前台客户发送来的消息
        User user = JSON.parseObject(message, User.class);
        RptRobotProblemEntity problem = new RptRobotProblemEntity();
        problem.setProblem(user.getMessage());
        // 2. 将发送来的问题与数据库的问题进行匹配
        RptRobotProblemEntity haveAnswer = isHaveAnswer(problem);
        // 3. 返回数据库的问题答案
        if (haveAnswer == null) {
            sendMessage("没有相关答案，请联系相关人员 !!!");
        } else {
            result = haveAnswer.getAnswer();
        }
        // 4. 推送给前端，显示在页面上
        sendMessage(result);
    }

    public void sendMessage(String message) {
        try {
            this.session.getBasicRemote().sendText(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public RptRobotProblemEntity isHaveAnswer(RptRobotProblemEntity problem){
        List<RptRobotProblemEntity> problems = null;
        try {
            problems = problemService.getProblem(problem);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (problems.size() != 0) {
            return problems.get(0);
        }
        return null;
    }

//    public void sendMessage(String message){
//        for (WebSocket webSocket : webSocketSet) {
//            log.info("【websocket消息】广播消息，message = {}",message);
//            try {
//                webSocket.session.getBasicRemote().sendText(message);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//    }
}
