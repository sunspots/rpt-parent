package com.company.project.service;

import com.company.project.entity.RptMatchingHistoryReportEntity;
import com.company.project.entity.RptMatchingReportEntity;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

public interface RptMatchingReportService {

    //查询管辖机关和地址是否匹配，匹配成功的企业
    Integer findMatchingReportByDominationAndAddress();

    //查询管辖机关和地址是否匹配，未匹配成功的企业
    Integer findNotMatchingReportByDominationAndAddress();

    //查询匹配报表
    PageInfo<RptMatchingReportEntity> pageInfo(RptMatchingReportEntity rptMatchingReportEntity);

    // 手工将匹配失败的信息改成匹配成功的数据添加到数据库中
    Integer addMatchingFailToSuccess(RptMatchingReportEntity rptMatchingReportEntity);

    // 根据id查询匹配结果信息
    RptMatchingReportEntity findMatchingReportById(Integer id);

    // 查询匹配度高的地址
    List<RptMatchingReportEntity> findHighMatchingAddress(Integer id);

    PageInfo<RptMatchingHistoryReportEntity> pageHistoryInfo(RptMatchingHistoryReportEntity rptMatchingHistoryReportEntity);

}
