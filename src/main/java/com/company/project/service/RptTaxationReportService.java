package com.company.project.service;

import com.company.project.entity.RptTaxationReportEntity;
import com.github.pagehelper.PageInfo;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface RptTaxationReportService {

    //查询税务报表
    PageInfo<RptTaxationReportEntity> findAllTaxationReport(RptTaxationReportEntity rptTaxationReportEntity);

    //通过社会信用代码查询在数据库是否已存在
    RptTaxationReportEntity findSocialCreditCodeIsHave(String socialCreditCode);

    //通过社会信用代码查询数据库中该条数据是否一致
    RptTaxationReportEntity findIdenticalBySocialCreditCode(RptTaxationReportEntity rptTaxationReportEntity);

    //添加税务报表
    Integer addTaxationReport(List<RptTaxationReportEntity> rptTaxationReportEntityList);

    //修改税务报表
    Integer updateTaxationReport(List<RptTaxationReportEntity> rptTaxationReportEntityList);

    //上传Excel
    List<RptTaxationReportEntity> taxationReportUploadExcel(MultipartFile upload,Integer reportStatus) throws IOException;

}
