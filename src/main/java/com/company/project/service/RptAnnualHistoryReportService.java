package com.company.project.service;

import com.company.project.entity.RptAnnualHistoryReportEntity;
import com.github.pagehelper.PageInfo;

public interface RptAnnualHistoryReportService {

    //查询工商历史报表
    PageInfo<RptAnnualHistoryReportEntity> findAllAnnualHistoryReport(RptAnnualHistoryReportEntity rptAnnualHistoryReportEntity);


}
