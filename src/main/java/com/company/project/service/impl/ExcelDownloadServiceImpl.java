package com.company.project.service.impl;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.fastjson.JSON;
import com.company.project.common.utils.ExcelTemplate;
import com.company.project.common.utils.FileUtil;
import com.company.project.entity.*;
import com.company.project.service.ExcelDownloadService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.*;

@Service
@Slf4j
public class ExcelDownloadServiceImpl implements ExcelDownloadService {

    // 匹配报表下载
    @Override
    public void matchingDownload(String title, List<RptMatchingReportEntity> rptMatchingReportEntityList, HttpServletResponse response) throws IOException {
//        OutputStream out = null;
//        BufferedOutputStream bos = null;
//        try {
//            String templateFileName = FileUtil.getPath() + "exceltemplate" + File.separator + "rptmatchingreport.xlsx";
//
//            response.setContentType("application/vnd.ms-excel");
//            response.setCharacterEncoding("utf-8");
//            String fileName = URLEncoder.encode(title + ".xlsx", "utf-8");
//            response.setHeader("Content-disposition", "attachment; filename=" + new String(fileName.getBytes("UTF-8"), "UTF-8"));
//
//            out = response.getOutputStream();
//            bos = new BufferedOutputStream(out);
//
//            //读取Excel
//            ExcelWriter excelWriter = EasyExcel.write(bos).withTemplate(templateFileName).build();
//            WriteSheet writeSheet = EasyExcel.writerSheet().build();
//
//            //customerDealerInfo 是我查询并需导出的数据，并且里面的字段和excel需要导出的字段对应
//            // 直接写入Excel数据
//
////            out = response.getOutputStream();
////            new BufferedOutputStream(new FileOutputStream());
//            excelWriter.fill(list, writeSheet);
//            excelWriter.finish();
//            bos.flush();
//
//        } catch (Exception e) {
//            // 重置response
//            response.reset();
//            response.setContentType("application/json");
//            response.setCharacterEncoding("utf-8");
//            Map<String, String> map = new HashMap<String, String>(16);
//            map.put("status", "failure");
//            map.put("message", "下载文件失败" + e.getMessage());
//            response.getWriter().println(JSON.toJSONString(map));
//        }
//
        try {
            //文件名称
            String fileName = title + ".xlsx";

            LinkedHashMap<Integer, LinkedList<String>> rows = new LinkedHashMap();
            // LinkedList<String> rptStatisticsReportRows = new LinkedList();

            int key = 1;
            for (RptMatchingReportEntity rptMatchingReportEntity : rptMatchingReportEntityList) {
                LinkedList<String> row = new LinkedList();

                row.add(String.valueOf(rptMatchingReportEntity.getId()));
                row.add(String.valueOf(rptMatchingReportEntity.getEnterpriseName()));
                row.add(String.valueOf(rptMatchingReportEntity.getSocialCreditCode()));
                row.add(String.valueOf(rptMatchingReportEntity.getRegistrationNumber()));
                row.add(String.valueOf(rptMatchingReportEntity.getDomination()));
                row.add(String.valueOf(rptMatchingReportEntity.getAddress()));
                row.add(String.valueOf(rptMatchingReportEntity.getContactNumber()));
                row.add(String.valueOf(rptMatchingReportEntity.getEstablishDate()));
                row.add(String.valueOf(rptMatchingReportEntity.getApprovalDate()));
                row.add(String.valueOf(rptMatchingReportEntity.getEnterpriseStatus()));
                row.add(String.valueOf(rptMatchingReportEntity.getLiaisonManName()));
                row.add(String.valueOf(rptMatchingReportEntity.getLiaisonManPhone()));
                row.add(String.valueOf(rptMatchingReportEntity.getCreateTime()));

                if (rptMatchingReportEntity.getReportStatus() == 1) {
                    row.add("已上报");
                } else if (rptMatchingReportEntity.getReportStatus() == 2) {
                    row.add("未上报");
                }

                if (rptMatchingReportEntity.getEnterpriseType() == 1) {
                    row.add("企业");
                } else if (rptMatchingReportEntity.getEnterpriseType() == 2) {
                    row.add("农合社");
                } else if (rptMatchingReportEntity.getEnterpriseType() == 3) {
                    row.add("个人");
                }


                if (rptMatchingReportEntity.getMatchingResult() == 1) {
                    row.add("匹配成功");
                } else if (rptMatchingReportEntity.getMatchingResult() == 2) {
                    row.add("匹配失败");
                }


                rows.put((key++), row);
            }

            if (rows.size() > 0) {
                //获取文件夹路径
                ClassPathResource resource = new ClassPathResource("exceltemplate/rptmatchingreport.xlsx");
                InputStream inputStream = resource.getInputStream();
                ExcelTemplate excel = new ExcelTemplate(inputStream);

                //替换模板row区域的${}值
                //LinkedHashMap<Integer, LinkedList<String>> rows = linkedHashMapRows(list);
                excel.addRowByExist(0, 2, 2, 3, rows, true);
                excel.save(response, fileName);
                log.info("导出成功！");
            }
            // return 1;
        } catch (Exception e) {
            log.error("导出失败！");
            e.printStackTrace();
            // return 0;
        }
    }

    // 匹配报表下载（有税务状态）
    @Override
    public void matchingDownloadEmpty(String title, List<RptQingdiaoReportEntity> rptMatchingReportEntityList, HttpServletResponse response) {

        try {
            //文件名称
            String fileName = title + ".xlsx";

            LinkedHashMap<Integer, LinkedList<String>> rows = new LinkedHashMap();
            // LinkedList<String> rptStatisticsReportRows = new LinkedList();

            int key = 1;
            for (RptQingdiaoReportEntity rptQingdiaoReportEntity : rptMatchingReportEntityList) {
                LinkedList<String> row = new LinkedList();

                row.add(String.valueOf(rptQingdiaoReportEntity.getId()));
                row.add(String.valueOf(rptQingdiaoReportEntity.getEnterpriseName()));
                row.add(String.valueOf(rptQingdiaoReportEntity.getSocialCreditCode()));
                row.add(String.valueOf(rptQingdiaoReportEntity.getLegalRepresentative()));
                row.add(String.valueOf(rptQingdiaoReportEntity.getBusinessPlace()));
                row.add(String.valueOf(rptQingdiaoReportEntity.getBusinessScope()));
                row.add(String.valueOf(rptQingdiaoReportEntity.getContacts()));
                row.add(String.valueOf(rptQingdiaoReportEntity.getContactInformation()));
                row.add(String.valueOf(rptQingdiaoReportEntity.getStatus()));

                if (rptQingdiaoReportEntity.getReportStatus() == 1) {
                    row.add("空号");
                } else if (rptQingdiaoReportEntity.getReportStatus() == 2) {
                    row.add("税务上报异常");
                } else if (rptQingdiaoReportEntity.getReportStatus() == 3) {
                    row.add("黑名单");
                }

                row.add(String.valueOf(rptQingdiaoReportEntity.getCreateTime()));

                rows.put((key++), row);
            }

            if (rows.size() > 0) {
                //获取文件夹路径
                ClassPathResource resource = new ClassPathResource("exceltemplate/rptqingdiaoreport.xlsx");
                InputStream inputStream = resource.getInputStream();
                ExcelTemplate excel = new ExcelTemplate(inputStream);

                //替换模板row区域的${}值
                //LinkedHashMap<Integer, LinkedList<String>> rows = linkedHashMapRows(list);
                excel.addRowByExist(0, 2, 2, 3, rows, true);
                excel.save(response, fileName);
                log.info("导出成功！");
            }
            // return 1;
        } catch (Exception e) {
            log.error("导出失败！");
            e.printStackTrace();
            // return 0;
        }
    }

    @Override
    public void annualHistoryDownload(String title, List<RptAnnualHistoryReportEntity> rptAnnualHistoryReportEntityList, HttpServletResponse response) {
        try {
            //文件名称
            String fileName = title + ".xlsx";

            LinkedHashMap<Integer, LinkedList<String>> rows = new LinkedHashMap();
            // LinkedList<String> rptStatisticsReportRows = new LinkedList();

            int key = 1;
            for (RptAnnualHistoryReportEntity rptAnnualHistoryReportEntity : rptAnnualHistoryReportEntityList) {
                LinkedList<String> row = new LinkedList();

                row.add(String.valueOf(rptAnnualHistoryReportEntity.getRegistrationAuthority()));
                row.add(String.valueOf(rptAnnualHistoryReportEntity.getSocialCreditCode()));
                row.add(String.valueOf(rptAnnualHistoryReportEntity.getRegistrationNumber()));
                row.add(String.valueOf(rptAnnualHistoryReportEntity.getRegistrationAuthority()));
                row.add(String.valueOf(rptAnnualHistoryReportEntity.getDomination()));
                row.add(String.valueOf(rptAnnualHistoryReportEntity.getAddress()));
                row.add(String.valueOf(rptAnnualHistoryReportEntity.getContactNumber()));
                row.add(String.valueOf(rptAnnualHistoryReportEntity.getEstablishDate()));
                row.add(String.valueOf(rptAnnualHistoryReportEntity.getApprovalDate()));
                row.add(String.valueOf(rptAnnualHistoryReportEntity.getEnterpriseStatus()));
                row.add(String.valueOf(rptAnnualHistoryReportEntity.getLiaisonManName()));
                row.add(String.valueOf(rptAnnualHistoryReportEntity.getLiaisonManPhone()));
                row.add(String.valueOf(rptAnnualHistoryReportEntity.getCreateTime()));


                if (rptAnnualHistoryReportEntity.getReportStatus() == 1) {
                    row.add("已上报");
                } else if (rptAnnualHistoryReportEntity.getReportStatus() == 2) {
                    row.add("未上报");
                }

                if (rptAnnualHistoryReportEntity.getEnterpriseType() == 1) {
                    row.add("企业");
                } else if (rptAnnualHistoryReportEntity.getEnterpriseType() == 2) {
                    row.add("农合社");
                } else if (rptAnnualHistoryReportEntity.getEnterpriseType() == 3) {
                    row.add("个人");
                }

                if (rptAnnualHistoryReportEntity.getMatching() == 1) {
                    row.add("已匹配");
                } else if (rptAnnualHistoryReportEntity.getMatching() == 2) {
                    row.add("未匹配");
                }

                rows.put((key++), row);
            }

            if (rows.size() > 0) {
                //获取文件夹路径
                ClassPathResource resource = new ClassPathResource("exceltemplate/matchinghistoryreport.xlsx");
                InputStream inputStream = resource.getInputStream();
                ExcelTemplate excel = new ExcelTemplate(inputStream);

                //替换模板row区域的${}值
                //LinkedHashMap<Integer, LinkedList<String>> rows = linkedHashMapRows(list);
                excel.addRowByExist(0, 2, 2, 3, rows, true);
                excel.save(response, fileName);
                log.info("导出成功！");
            }
            // return 1;
        } catch (Exception e) {
            log.error("导出失败！");
            e.printStackTrace();
            // return 0;
        }
    }

    @Override
    public void rptApprovalHistoryDownload(String title, List<RptApprovalHistoryReportEntity> rptApprovalHistoryReportEntityList, HttpServletResponse response) {
        try {
            //文件名称
            String fileName = title + ".xlsx";

            LinkedHashMap<Integer, LinkedList<String>> rows = new LinkedHashMap();
            // LinkedList<String> rptStatisticsReportRows = new LinkedList();

            int key = 1;
            for (RptApprovalHistoryReportEntity rptApprovalHistoryReportEntity : rptApprovalHistoryReportEntityList) {
                LinkedList<String> row = new LinkedList();

                row.add(String.valueOf(rptApprovalHistoryReportEntity.getRegistrationAuthority()));
                row.add(String.valueOf(rptApprovalHistoryReportEntity.getDomination()));
                row.add(String.valueOf(rptApprovalHistoryReportEntity.getAddress()));
                row.add(String.valueOf(rptApprovalHistoryReportEntity.getCreateTime()));

                rows.put((key++), row);
            }

            if (rows.size() > 0) {
                //获取文件夹路径
                ClassPathResource resource = new ClassPathResource("exceltemplate/approvalhistory.xlsx");
                InputStream inputStream = resource.getInputStream();
                ExcelTemplate excel = new ExcelTemplate(inputStream);

                //替换模板row区域的${}值
                //LinkedHashMap<Integer, LinkedList<String>> rows = linkedHashMapRows(list);
                excel.addRowByExist(0, 2, 2, 3, rows, true);
                excel.save(response, fileName);
                log.info("导出成功！");
            }
            // return 1;
        } catch (Exception e) {
            log.error("导出失败！");
            e.printStackTrace();
            // return 0;
        }
    }

    @Override
    public void rptTaxationHistoryDownload(String title, List<RptTaxationHistoryReportEntity> rptTaxationHistoryReportEntityList, HttpServletResponse response) {
        try {
            //文件名称
            String fileName = title + ".xlsx";

            LinkedHashMap<Integer, LinkedList<String>> rows = new LinkedHashMap();
            // LinkedList<String> rptStatisticsReportRows = new LinkedList();

            int key = 1;
            for (RptTaxationHistoryReportEntity rptTaxationHistoryReportEntity : rptTaxationHistoryReportEntityList) {
                LinkedList<String> row = new LinkedList();

                row.add(String.valueOf(rptTaxationHistoryReportEntity.getEnterpriseName()));
                row.add(String.valueOf(rptTaxationHistoryReportEntity.getSocialCreditCode()));
                row.add(String.valueOf(rptTaxationHistoryReportEntity.getLegalRepresentative()));
                row.add(String.valueOf(rptTaxationHistoryReportEntity.getBusinessPlace()));
                row.add(String.valueOf(rptTaxationHistoryReportEntity.getBusinessScope()));
                row.add(String.valueOf(rptTaxationHistoryReportEntity.getContacts()));
                row.add(String.valueOf(rptTaxationHistoryReportEntity.getContactInformation()));
                row.add(String.valueOf(rptTaxationHistoryReportEntity.getStatus()));

                if (rptTaxationHistoryReportEntity.getReportStatus() == 1) {
                    row.add("正常");
                } else if (rptTaxationHistoryReportEntity.getReportStatus() == 2) {
                    row.add("税务异常");
                } else if (rptTaxationHistoryReportEntity.getReportStatus() == 3) {
                    row.add("黑名单");
                }

                row.add(String.valueOf(rptTaxationHistoryReportEntity.getCreateTime()));
                rows.put((key++), row);
            }

            if (rows.size() > 0) {
                //获取文件夹路径
                ClassPathResource resource = new ClassPathResource("exceltemplate/taxationhistory.xlsx");
                InputStream inputStream = resource.getInputStream();
                ExcelTemplate excel = new ExcelTemplate(inputStream);

                //替换模板row区域的${}值
                //LinkedHashMap<Integer, LinkedList<String>> rows = linkedHashMapRows(list);
                excel.addRowByExist(0, 2, 2, 3, rows, true);
                excel.save(response, fileName);
                log.info("导出成功！");
            }
            // return 1;
        } catch (Exception e) {
            log.error("导出失败！");
            e.printStackTrace();
            // return 0;
        }
    }

    @Override
    public void matchingHistoryDownload(String title, List<RptMatchingHistoryReportEntity> rptMatchingHistoryReportEntityList, HttpServletResponse response) {
        try {
            //文件名称
            String fileName = title + ".xlsx";

            LinkedHashMap<Integer, LinkedList<String>> rows = new LinkedHashMap();
            // LinkedList<String> rptStatisticsReportRows = new LinkedList();

            int key = 1;
            for (RptMatchingHistoryReportEntity rptMatchingHistoryReportEntity : rptMatchingHistoryReportEntityList) {

                LinkedList<String> row = new LinkedList();
                row.add(String.valueOf(rptMatchingHistoryReportEntity.getEnterpriseName()));
                row.add(String.valueOf(rptMatchingHistoryReportEntity.getSocialCreditCode()));
                row.add(String.valueOf(rptMatchingHistoryReportEntity.getRegistrationNumber()));
                row.add(String.valueOf(rptMatchingHistoryReportEntity.getDomination()));
                row.add(String.valueOf(rptMatchingHistoryReportEntity.getAddress()));
                row.add(String.valueOf(rptMatchingHistoryReportEntity.getContactNumber()));
                row.add(String.valueOf(rptMatchingHistoryReportEntity.getEstablishDate()));
                row.add(String.valueOf(rptMatchingHistoryReportEntity.getApprovalDate()));
                row.add(String.valueOf(rptMatchingHistoryReportEntity.getEnterpriseStatus()));
                row.add(String.valueOf(rptMatchingHistoryReportEntity.getLiaisonManName()));

                row.add(String.valueOf(rptMatchingHistoryReportEntity.getLiaisonManName()));
                row.add(String.valueOf(rptMatchingHistoryReportEntity.getLiaisonManPhone()));
                row.add(String.valueOf(rptMatchingHistoryReportEntity.getCreateTime()));

                if (rptMatchingHistoryReportEntity.getReportStatus() == 1) {
                    row.add("已上报");
                } else if (rptMatchingHistoryReportEntity.getReportStatus() == 2) {
                    row.add("未上报");
                }

                if (rptMatchingHistoryReportEntity.getMatchingResult() == 1) {
                    row.add("匹配成功");
                } else if (rptMatchingHistoryReportEntity.getMatchingResult() == 2) {
                    row.add("匹配失败");
                }


                if (rptMatchingHistoryReportEntity.getConfirmStatus() == 1) {
                    row.add("已确认");
                } else if (rptMatchingHistoryReportEntity.getConfirmStatus() == 2) {
                    row.add("未确认");
                }



                rows.put((key++), row);

            }

            if (rows.size() > 0) {
                //获取文件夹路径
                ClassPathResource resource = new ClassPathResource("exceltemplate/matchinghistoryreport.xlsx");
                InputStream inputStream = resource.getInputStream();
                ExcelTemplate excel = new ExcelTemplate(inputStream);

                //替换模板row区域的${}值
                //LinkedHashMap<Integer, LinkedList<String>> rows = linkedHashMapRows(list);
                excel.addRowByExist(0, 2, 2, 3, rows, true);
                excel.save(response, fileName);
                log.info("导出成功！");
            }
            // return 1;
        } catch (Exception e) {
            log.error("导出失败！");
            e.printStackTrace();
            // return 0;
        }
    }

    @Override
    public void qingDiaoHistoryDownload(String title, List<RptQingdiaoHistoryReportEntity> rptQingdiaoHistoryReportEntityList, HttpServletResponse response) {
        try {
            //文件名称
            String fileName = title + ".xlsx";

            LinkedHashMap<Integer, LinkedList<String>> rows = new LinkedHashMap();
            // LinkedList<String> rptStatisticsReportRows = new LinkedList();

            int key = 1;
            for (RptQingdiaoHistoryReportEntity rptQingdiaoHistoryReportEntity : rptQingdiaoHistoryReportEntityList) {

                LinkedList<String> row = new LinkedList();

                row.add(String.valueOf(rptQingdiaoHistoryReportEntity.getEnterpriseName()));
                row.add(String.valueOf(rptQingdiaoHistoryReportEntity.getSocialCreditCode()));
                row.add(String.valueOf(rptQingdiaoHistoryReportEntity.getLegalRepresentative()));
                row.add(String.valueOf(rptQingdiaoHistoryReportEntity.getBusinessPlace()));
                row.add(String.valueOf(rptQingdiaoHistoryReportEntity.getBusinessScope()));
                row.add(String.valueOf(rptQingdiaoHistoryReportEntity.getContacts()));
                row.add(String.valueOf(rptQingdiaoHistoryReportEntity.getContactInformation()));
                row.add(String.valueOf(rptQingdiaoHistoryReportEntity.getStatus()));

                if (rptQingdiaoHistoryReportEntity.getReportStatus() == 1) {
                    row.add("空号");
                } else if (rptQingdiaoHistoryReportEntity.getReportStatus() == 2) {
                    row.add("税务上报异常");
                } else if (rptQingdiaoHistoryReportEntity.getReportStatus() == 3) {
                    row.add("黑名单");
                }

                rows.put((key++), row);

            }

            if (rows.size() > 0) {
                //获取文件夹路径
                ClassPathResource resource = new ClassPathResource("exceltemplate/QingdiaoHistoryReport.xlsx");
                InputStream inputStream = resource.getInputStream();
                ExcelTemplate excel = new ExcelTemplate(inputStream);

                //替换模板row区域的${}值
                //LinkedHashMap<Integer, LinkedList<String>> rows = linkedHashMapRows(list);
                excel.addRowByExist(0, 2, 2, 3, rows, true);
                excel.save(response, fileName);
                log.info("导出成功！");
            }
            // return 1;
        } catch (Exception e) {
            log.error("导出失败！");
            e.printStackTrace();
            // return 0;
        }
    }


    // 统计报表-查询所有统计数据（不包括清吊和空号）
    @Override
    public void statisticsReportExcel(String title, List<RptStatisticsReportEntity> rptStatisticsReportEntityList, HttpServletResponse response) {

        try {
            //文件名称
            String fileName = title + ".xlsx";

            LinkedHashMap<Integer, LinkedList<String>> rows = new LinkedHashMap();
            // LinkedList<String> rptStatisticsReportRows = new LinkedList();

            int key = 1;
            for (RptStatisticsReportEntity rptStatisticsReportEntity : rptStatisticsReportEntityList) {
                if (rptStatisticsReportEntity.getStatus() == 1) {
                    LinkedList<String> row = new LinkedList();
                    row.add(String.valueOf(rptStatisticsReportEntity.getAddressName()));
                    row.add(String.valueOf(rptStatisticsReportEntity.getAnnualReport()));
                    row.add(String.valueOf(rptStatisticsReportEntity.getReportedQuantity()));
                    row.add(String.valueOf(rptStatisticsReportEntity.getNotReportQuantity()));
                    row.add(String.valueOf(rptStatisticsReportEntity.getAnnualReportRate()));
                    row.add(String.valueOf(rptStatisticsReportEntity.getRanking()));

                    rows.put((key++), row);
                }
            }

            if (rows.size() > 0) {
                //获取文件夹路径
                ClassPathResource resource = new ClassPathResource("exceltemplate/streetReport.xlsx");
                InputStream inputStream = resource.getInputStream();
                ExcelTemplate excel = new ExcelTemplate(inputStream);

                //替换模板row区域的${}值
                //LinkedHashMap<Integer, LinkedList<String>> rows = linkedHashMapRows(list);
                excel.addRowByExist(0, 3, 3, 4, rows, true);
                excel.save(response, fileName);
                log.info("导出成功！");
            }
            // return 1;
        } catch (Exception e) {
            log.error("导出失败！");
            e.printStackTrace();
            // return 0;
        }
    }

    // 统计报表-查询所有统计数据（包括清吊和空号）
    @Override
    public void statisticsReportExcel1(String title, List<RptStatisticsReportEntity> rptStatisticsReportEntityList, HttpServletResponse response) {

        try {
            //文件名称
            String fileName = title + ".xlsx";

            LinkedHashMap<Integer, LinkedList<String>> rows = new LinkedHashMap();
            // LinkedList<String> rptStatisticsReportRows = new LinkedList();

            int key = 1;
            for (RptStatisticsReportEntity rptStatisticsReportEntity : rptStatisticsReportEntityList) {
                if (rptStatisticsReportEntity.getStatus() == 2) {
                    LinkedList<String> row = new LinkedList();

                    row.add(String.valueOf(rptStatisticsReportEntity.getAddressName()));
                    row.add(String.valueOf(rptStatisticsReportEntity.getAnnualReport()));
                    row.add(String.valueOf(rptStatisticsReportEntity.getReportedQuantity()));
                    row.add(String.valueOf(rptStatisticsReportEntity.getNotReportQuantity()));
                    row.add(String.valueOf(rptStatisticsReportEntity.getEmptyNumber()));
                    row.add(String.valueOf(rptStatisticsReportEntity.getClearNumber()));
                    row.add(String.valueOf(rptStatisticsReportEntity.getAnnualReportRate()));
                    row.add(String.valueOf(rptStatisticsReportEntity.getRanking()));

                    rows.put((key++), row);

                }

            }

            if (rows.size() > 0) {
                //获取文件夹路径
                ClassPathResource resource = new ClassPathResource("exceltemplate/streetReportEmpty.xlsx");
                InputStream inputStream = resource.getInputStream();
                ExcelTemplate excel = new ExcelTemplate(inputStream);

                //替换模板row区域的${}值
                //LinkedHashMap<Integer, LinkedList<String>> rows = linkedHashMapRows(list);
                excel.addRowByExist(0, 3, 3, 4, rows, true);
                excel.save(response, fileName);
                log.info("导出成功！");
            }
            // return 1;
        } catch (Exception e) {
            log.error("导出失败！");
            e.printStackTrace();
            // return 0;
        }
    }
}
