package com.company.project.service.impl;

import com.company.project.entity.RptAnnualHistoryReportEntity;
import com.company.project.mapper.RptAnnualHistoryReportMapper;
import com.company.project.service.RptAnnualHistoryReportService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RptAnnualHistoryReportServiceImpl implements RptAnnualHistoryReportService {

    @Autowired
    RptAnnualHistoryReportMapper rptAnnualHistoryReportMapper;

    //查询工商历史报表
    @Override
    public PageInfo<RptAnnualHistoryReportEntity> findAllAnnualHistoryReport(RptAnnualHistoryReportEntity rptAnnualHistoryReportEntity) {
        PageHelper.startPage(rptAnnualHistoryReportEntity.getPage(), rptAnnualHistoryReportEntity.getLimit());
        List<RptAnnualHistoryReportEntity> list = rptAnnualHistoryReportMapper.findAllAnnualHistoryReport(rptAnnualHistoryReportEntity);
        PageInfo<RptAnnualHistoryReportEntity> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }


}
