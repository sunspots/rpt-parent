package com.company.project.service.impl;

import com.company.project.entity.RptApprovalHistoryReportEntity;
import com.company.project.mapper.RptApprovalHistoryReportMapper;
import com.company.project.service.RptApprovalHistoryReportService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RptApprovalHistoryReportServiceImpl implements RptApprovalHistoryReportService {

    @Autowired
    private RptApprovalHistoryReportMapper rptApprovalHistoryReportMapper;

    //查询核准历史报表
    @Override
    public PageInfo<RptApprovalHistoryReportEntity> findAllApprovalHistoryReport(RptApprovalHistoryReportEntity rptApprovalHistoryReportEntity) {

        PageHelper.startPage(rptApprovalHistoryReportEntity.getPage(), rptApprovalHistoryReportEntity.getLimit());
        List<RptApprovalHistoryReportEntity> list = rptApprovalHistoryReportMapper.findAllApprovalHistoryReport(rptApprovalHistoryReportEntity);
        PageInfo<RptApprovalHistoryReportEntity> pageInfo = new PageInfo<>(list);

        return pageInfo;
    }




}
