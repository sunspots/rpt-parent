package com.company.project.service.impl;

import com.company.project.entity.*;
import com.company.project.mapper.RptAnnualReportMapper;
import com.company.project.mapper.RptMatchingReportMapper;
import com.company.project.mapper.RptStatisticsReportMapper;
import com.company.project.service.RptStatisticsReportService;
import com.company.project.service.RptTaxationReportService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class RptStatisticsReportServiceImpl implements RptStatisticsReportService {

    @Autowired
    private RptAnnualReportMapper rptAnnualReportMapper;

    @Autowired
    private RptStatisticsReportMapper rptStatisticsReportMapper;

    @Autowired
    private RptMatchingReportMapper rptMatchingReportMapper;

    /**
     * 从工商系统的表根据街道的名称获取不同街道不同企业类型的年报
     *
     * @return
     */
    @Override
    public Map<String, Object> findAddressAnnualReportByAddress() {
        // 查找街道所有集合
        List<String> addrList = rptAnnualReportMapper.findAddressName();

        // 存放排序完街道的数据
        Map<String, Object> map = new HashMap<>();

        // 遍历企业类型
        for (int i = 1; i < 4; i++) {
            // 存放各个街道数据的集合
            List<RptStatisticsReportEntity> rptStatisticsReportEntityList = new ArrayList<>();
            // 遍历地址
            for (String str : addrList) {
                // 将街道封装到对象中
                RptStatisticsReportEntity rptStatisticsReportEntity = new RptStatisticsReportEntity();
                rptStatisticsReportEntity.setAddressName(str);
                rptStatisticsReportEntity.setEnterpriseType(i);
                rptStatisticsReportEntity.setStatus(1);
                rptStatisticsReportEntity.setCreateTime(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
                // 将对象传入到数据库，查找该街道下的类型企业总数量
                List<RptAnnualReportEntity> addressAnnualReportByAddress = rptAnnualReportMapper.findAddressAnnualReportByAddress(rptStatisticsReportEntity);
                if (addressAnnualReportByAddress.size() == 0) {
                    rptStatisticsReportEntity.setAnnualReport(0);
                    rptStatisticsReportEntity.setReportedQuantity(0);
                    rptStatisticsReportEntity.setNotReportQuantity(0);
                    rptStatisticsReportEntity.setAnnualReportRate("0");
                } else {
                    // 封装传回来的数据
                    rptStatisticsReportEntity.setAnnualReport(addressAnnualReportByAddress.size()); // 街道年报总数

                    // 获取该街道年报上报的企业
                    List<RptAnnualReportEntity> reportList = rptAnnualReportMapper.findAddressAnnualReportByReportStatus(rptStatisticsReportEntity);
                    // 企业上报年报数量
                    rptStatisticsReportEntity.setReportedQuantity(reportList.size());
                    // 企业未上报年报数量
                    rptStatisticsReportEntity.setNotReportQuantity(rptStatisticsReportEntity.getAnnualReport() - reportList.size());

                    // 计算年报率
                    Double rate = (Double.valueOf(rptStatisticsReportEntity.getReportedQuantity()) / Double.valueOf(rptStatisticsReportEntity.getAnnualReport())) * 100;
                    // 取小数点后两位
                    String string = rate.toString();
                    int index = string.indexOf(".");
                    String substring = string.substring(0, index + 2);
                    rptStatisticsReportEntity.setAnnualReportRate(substring);
                }
                rptStatisticsReportEntityList.add(rptStatisticsReportEntity);
            }
            // 根据街道年报率进行排序行排序
            List<RptStatisticsReportEntity> resultList = sortReportRate(rptStatisticsReportEntityList);
            map.put("type" + i, resultList);
            // 添加到数据库中
            Integer report = addStatisticsReport(resultList);
        }
        return map;
    }

    /**
     * 从工商系统的表根据街道的名称获取不同街道不同企业类型的年报（包括空号和清吊）
     *
     * @return
     */
    @Override
    public Map<String, Object> findEmptyAndClearNumberReportByAddress() {
        // 查找街道所有集合
        List<String> addrList = rptAnnualReportMapper.findAddressName();

        // 存放排序完街道的数据
        Map<String, Object> map = new HashMap<>();

        // 遍历企业类型
        for (int i = 1; i < 4; i++) {
            // 存放各个街道数据的集合
            List<RptStatisticsReportEntity> rptStatisticsReportEntityList = new ArrayList<>();
            // 遍历地址
            for (String str : addrList) {
                RptTaxationReportEntity rptTaxationReportEntity = new RptTaxationReportEntity();
                rptTaxationReportEntity.setBusinessPlace(str);
                // 将街道封装到对象中
                RptStatisticsReportEntity rptStatisticsReportEntity = new RptStatisticsReportEntity();
                rptStatisticsReportEntity.setAddressName(str);
                rptStatisticsReportEntity.setEnterpriseType(i);
                rptStatisticsReportEntity.setCreateTime(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
                rptStatisticsReportEntity.setStatus(2);
                // 将对象传入到数据库，查找该街道下的类型企业总数量
                List<RptAnnualReportEntity> addressAnnualReportByAddress = rptAnnualReportMapper.findAddressAnnualReportByAddress(rptStatisticsReportEntity);
                if (addressAnnualReportByAddress.size() == 0) {
                    rptStatisticsReportEntity.setAnnualReport(0);
                    rptStatisticsReportEntity.setReportedQuantity(0);
                    rptStatisticsReportEntity.setNotReportQuantity(0);
                    rptStatisticsReportEntity.setAnnualReportRate("0");
                    rptStatisticsReportEntity.setEmptyNumber(0);
                    rptStatisticsReportEntity.setClearNumber(0);
                } else {
                    // 封装传回来的数据
                    rptStatisticsReportEntity.setAnnualReport(addressAnnualReportByAddress.size()); // 街道年报总数

                    // 获取该街道年报上报的企业
                    List<RptAnnualReportEntity> reportList = rptAnnualReportMapper.findAddressAnnualReportByReportStatus(rptStatisticsReportEntity);
                    // 企业上报年报数量
                    rptStatisticsReportEntity.setReportedQuantity(reportList.size());
                    // 企业未上报年报数量
                    rptStatisticsReportEntity.setNotReportQuantity(rptStatisticsReportEntity.getAnnualReport() - reportList.size());

                    // 获取空号数量
                    List<RptTaxationReportEntity> emptyNumbers = rptAnnualReportMapper.findEmptyNumberByAddress(rptTaxationReportEntity);
                    rptStatisticsReportEntity.setEmptyNumber(emptyNumbers.size());

                    // 获取清吊数量
                    List<RptTaxationReportEntity> clearNumbers = rptAnnualReportMapper.findClearNumberByAddress(rptTaxationReportEntity);
                    rptStatisticsReportEntity.setClearNumber(clearNumbers.size());

                    // 计算年报率
                    Double rate = (Double.valueOf(rptStatisticsReportEntity.getReportedQuantity() + rptStatisticsReportEntity.getClearNumber() + rptStatisticsReportEntity.getEmptyNumber())
                            / Double.valueOf(rptStatisticsReportEntity.getAnnualReport())) * 100;
                    // 取小数点后两位
                    String string = rate.toString();
                    int index = string.indexOf(".");
                    String substring = string.substring(0, index + 2);
                    rptStatisticsReportEntity.setAnnualReportRate(substring);
                }
                rptStatisticsReportEntityList.add(rptStatisticsReportEntity);
            }
            // 根据街道年报率进行排序行排序
            List<RptStatisticsReportEntity> resultList = sortReportRate(rptStatisticsReportEntityList);
            // 添加到数据库中
            Integer report = addStatisticsReport(resultList);
            map.put("type" + (i+3), resultList);
        }
        return map;
    }

    @Override
    public Map<String, Object> findAllStatisticsReport() {
        Map<String, Object> map = new HashMap<>();
        List<RptStatisticsReportEntity> list = rptStatisticsReportMapper.findAllStatisticsReport();
        // 企业类型
        for (int i = 1; i < 4; i++) {
            List<RptStatisticsReportEntity> result = new ArrayList<>();
            for (RptStatisticsReportEntity r : list) {
                if (r.getEnterpriseType() != i) continue;
                if (r.getStatus() == 1) {
                    result.add(r);
                }
            }
            map.put("type" + i, result);
        }
        //企业类型（包含清吊和空号）
        for (int i = 1; i < 4; i++) {
            List<RptStatisticsReportEntity> result = new ArrayList<>();
            for (RptStatisticsReportEntity r : list) {
                if (r.getEnterpriseType() != i) continue;
                if (r.getStatus() == 2) {
                    result.add(r);
                }
            }
            map.put("type" + (i+3), result);
        }
        return map;
    }

    /**
     * 统计历史表模糊查询
     * @param rptStatisticsHistoryReportEntity
     * @return
     */
    @Override
    public PageInfo<RptStatisticsHistoryReportEntity> findStatisticsHistory(RptStatisticsHistoryReportEntity rptStatisticsHistoryReportEntity) {
        PageHelper.startPage(rptStatisticsHistoryReportEntity.getPage(), rptStatisticsHistoryReportEntity.getLimit());
        List<RptStatisticsHistoryReportEntity> list = rptStatisticsReportMapper.findStatisticsHistory(rptStatisticsHistoryReportEntity);
        PageInfo<RptStatisticsHistoryReportEntity> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    /**
     * 统计历史表模糊查询(含清吊和空号)
     * @param rptStatisticsHistoryReportEntity
     * @return
     */
    @Override
    public PageInfo<RptStatisticsHistoryReportEntity> findStatisticsHistory1(RptStatisticsHistoryReportEntity rptStatisticsHistoryReportEntity) {
        PageHelper.startPage(rptStatisticsHistoryReportEntity.getPage(), rptStatisticsHistoryReportEntity.getLimit());
        List<RptStatisticsHistoryReportEntity> list = rptStatisticsReportMapper.findStatisticsHistory1(rptStatisticsHistoryReportEntity);
        PageInfo<RptStatisticsHistoryReportEntity> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }


    /**
     * 把数据添加到数据库中（包括新增和更新）
     *
     * @param list
     * @return
     */
    private Integer addStatisticsReport(List<RptStatisticsReportEntity> list) {
        Integer result = 0;
        for (RptStatisticsReportEntity rptStatisticsReportEntity : list) {
            if (rptStatisticsReportMapper.findStatisticsReportByDateAndStatus(rptStatisticsReportEntity) != null) {
                // 存在该数据，对其更新
                result = rptStatisticsReportMapper.updateStatisticsReport(rptStatisticsReportEntity);
            } else {
                // 不存在该数据，对其增加
                result = rptStatisticsReportMapper.addStatisticsReport(rptStatisticsReportEntity);
            }
        }
        return result;
    }

    /**
     * 定时任务：将统计报表存入历史表中
     */
    @Scheduled(cron = "0 59 23 * * ? ")
    public void autoSetStatisticsReportToHistory(){
        List<RptStatisticsReportEntity> list = rptStatisticsReportMapper.findAllStatisticsReport();
        if (list.size() != 0) {
            // 将数据删除
            rptStatisticsReportMapper.deleteAllStatisticsReport();
            // 将数据存入至历史表中
            rptStatisticsReportMapper.autoSetMatchingReportToHistory(list);
        }
    }

    /**
     * 对年报率进行排序，
     *
     * @param rptStatisticsList
     * @return
     */
    private List<RptStatisticsReportEntity> sortReportRate(List<RptStatisticsReportEntity> rptStatisticsList) {
        // 对查出来的年报进行降序排序
        Collections.sort(rptStatisticsList, new Comparator<RptStatisticsReportEntity>() {
            @Override
            public int compare(RptStatisticsReportEntity o1, RptStatisticsReportEntity o2) {
                if (Double.parseDouble(o1.getAnnualReportRate()) > Double.parseDouble(o2.getAnnualReportRate())) {
                    return -1;
                }
                if (o1.getAnnualReportRate().equals(o2.getAnnualReportRate())) {
                    return 0;
                }
                return 1;

            }
        });
        // 设置排名
        for (int i = 0; i < rptStatisticsList.size(); i++) {
            rptStatisticsList.get(i).setRanking(i + 1);
        }
        return rptStatisticsList;
    }
}
