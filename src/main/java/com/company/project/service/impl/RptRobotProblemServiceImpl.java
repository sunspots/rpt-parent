package com.company.project.service.impl;

import com.company.project.entity.RptRobotProblemEntity;
import com.company.project.mapper.RptRobotProblemMapper;
import com.company.project.service.RptRobotProblemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("rptRobotProblemService")
public class RptRobotProblemServiceImpl implements RptRobotProblemService {

    @Autowired
    private RptRobotProblemMapper rptRobotProblemMapper;

    @Override
    public Integer addProblem(RptRobotProblemEntity problem) {
        return rptRobotProblemMapper.addProblem(problem);
    }

    @Override
    public Integer updateProblem(RptRobotProblemEntity problem) {
        return rptRobotProblemMapper.updateProblem(problem);
    }

    @Override
    public RptRobotProblemEntity getProblemById(Integer id) {
        return rptRobotProblemMapper.getProblemById(id);
    }

    @Override
    public List<RptRobotProblemEntity> getAllProblem() {
        return rptRobotProblemMapper.getAllProblem();
    }

    @Override
    public List<RptRobotProblemEntity> getProblem(RptRobotProblemEntity problem) {
        return rptRobotProblemMapper.getProblem(problem);
    }
}
