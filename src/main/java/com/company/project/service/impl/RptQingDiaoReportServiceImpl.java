package com.company.project.service.impl;

import com.company.project.entity.RptAnnualReportEntity;
import com.company.project.entity.RptQingdiaoReportEntity;
import com.company.project.entity.RptTaxationReportEntity;
import com.company.project.mapper.RptQingDiaoHistoryReportMapper;
import com.company.project.mapper.RptQingDiaoReportMapper;
import com.company.project.service.RptQingDiaoReportService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class RptQingDiaoReportServiceImpl implements RptQingDiaoReportService {

    @Autowired
    private RptQingDiaoReportMapper rptQingDiaoReportMapper;

    @Autowired
    private RptQingDiaoHistoryReportMapper rptQingDiaoHistoryReportMapper;

    /**
     * 查询清吊报表
     * @param rptQingdiaoReportEntity
     * @return
     */
    @Override
    public PageInfo<RptQingdiaoReportEntity> findAllQingDiaoReport(RptQingdiaoReportEntity rptQingdiaoReportEntity) {

        PageHelper.startPage(rptQingdiaoReportEntity.getPage(), rptQingdiaoReportEntity.getLimit());
        List<RptQingdiaoReportEntity> list = rptQingDiaoReportMapper.findAllQingDiaoReport(rptQingdiaoReportEntity);
        PageInfo<RptQingdiaoReportEntity> pageInfo = new PageInfo<>(list);

        return pageInfo;
    }

    /**
     * 通过社会信用代码查询在数据库是否已存在
     * @param socialCreditCode
     * @return
     */
    @Override
    public RptQingdiaoReportEntity findSocialCreditCodeIsHave(String socialCreditCode) {

        return rptQingDiaoReportMapper.findSocialCreditCodeIsHave(socialCreditCode);
    }

    /**
     * 通过社会信用代码查询数据库中该条数据是否一致
     * @param rptQingdiaoReportEntity
     * @return
     */
    @Override
    public RptQingdiaoReportEntity findIdenticalBySocialCreditCode(RptQingdiaoReportEntity rptQingdiaoReportEntity) {

        return rptQingDiaoReportMapper.findIdenticalBySocialCreditCode(rptQingdiaoReportEntity);
    }

    /**
     * 点击触发清吊事件
     * @return
     */
    public Integer triggerQingDiaoForTaxationReport() throws ParseException {

        //创建清吊信息集合
        List<RptQingdiaoReportEntity> rptQingdiaoReportEntityList = new ArrayList<>();

        //获取税务报表税务状态为黑名单的数据
        List<RptTaxationReportEntity> list = rptQingDiaoReportMapper.findTaxationReportForThree();
        //获取税务报表以社会信用代码为标识与工商报表匹配出税务状态为税务异常,上报状态为未上报的数据
        List<RptTaxationReportEntity> list1 = rptQingDiaoReportMapper.findTaxationReportForTwo();
        //获取工商报表以社会信用代码为标识与税务报表匹配出税务状态为税务异常,上报状态为未上报的数据
        List<RptAnnualReportEntity> list2 = rptQingDiaoReportMapper.findAnnualReportForTwo();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String currentDate = simpleDateFormat.format(new Date());
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();

        for (int i = 0; i < list1.size(); i++) {
            //获取税务报表以社会信用代码为标识与工商报表匹配出税务状态为税务异常,上报状态为未上报的数据的社会信用代码
            String socialCreditCode = list1.get(i).getSocialCreditCode();
            for (int j = 0; j < list2.size(); j++) {
                //获取工商报表以社会信用代码为标识与税务报表匹配出税务状态为税务异常,上报状态为未上报的数据的社会信用代码
                String socialCreditCode1 = list2.get(j).getSocialCreditCode();
                //获取工商报表以社会信用代码为标识与税务报表匹配出税务状态为税务异常,上报状态为未上报的数据的核准日期
                String approvalDate = list2.get(j).getApprovalDate();

                calendar1.setTime(simpleDateFormat.parse(currentDate));
                calendar2.setTime(simpleDateFormat.parse(approvalDate));
                int date = calendar1.get(Calendar.DATE) - calendar2.get(Calendar.DATE);
                int month = calendar1.get(Calendar.MONTH) - calendar2.get(Calendar.MONTH);
                int year = calendar1.get(Calendar.YEAR) - calendar2.get(Calendar.YEAR);
                if(month < 0){
                    month = 0;
                }else if(month == 0){
                    month = date < 0 ? 0 : 1;
                }else{
                    month = 1;
                }
                int result = month + year;
                //社会信用代码相等
                if (socialCreditCode.equals(socialCreditCode1)){
                    //相差年限大于2
                    if (result > 2){
                        list.add(list1.get(i));
                        break;
                    }else {
                        break;
                    }
                }
            }
        }

        for (int i = 0; i < list.size(); i++) {
            RptQingdiaoReportEntity rptQingdiaoReportEntity = new RptQingdiaoReportEntity();
            rptQingdiaoReportEntity.setEnterpriseName(list.get(i).getEnterpriseName());
            rptQingdiaoReportEntity.setSocialCreditCode(list.get(i).getSocialCreditCode());
            rptQingdiaoReportEntity.setLegalRepresentative(list.get(i).getLegalRepresentative());
            rptQingdiaoReportEntity.setBusinessPlace(list.get(i).getBusinessPlace());
            rptQingdiaoReportEntity.setBusinessScope(list.get(i).getBusinessScope());
            rptQingdiaoReportEntity.setContacts(list.get(i).getContacts());
            rptQingdiaoReportEntity.setContactInformation(list.get(i).getContactInformation());
            rptQingdiaoReportEntity.setStatus(list.get(i).getStatus());
            rptQingdiaoReportEntity.setReportStatus(list.get(i).getReportStatus());
            rptQingdiaoReportEntity.setCreateTime(list.get(i).getCreateTime());
            rptQingdiaoReportEntity.setText1(list.get(i).getText1());
            rptQingdiaoReportEntity.setText2(list.get(i).getText2());
            rptQingdiaoReportEntity.setText3(list.get(i).getText3());
            rptQingdiaoReportEntityList.add(rptQingdiaoReportEntity);
        }

        List<RptQingdiaoReportEntity> addList = new ArrayList<>();//执行添加集合
        List<RptQingdiaoReportEntity> updateList = new ArrayList<>();//执行修改集合

        for (int i = 0; i < rptQingdiaoReportEntityList.size(); i++) {

            //获取清吊数据集合的社会信用代码
            String socialCreditCode = rptQingdiaoReportEntityList.get(i).getSocialCreditCode();

            //通过社会信用代码查询数据库是否已存在该数据
            if (rptQingDiaoReportMapper.findSocialCreditCodeIsHave(socialCreditCode) == null){
                //如果不存在,添加到addList集合
                addList.add(rptQingdiaoReportEntityList.get(i));
            }else {
                //如果存在，再通过社会信用代码查询数据库中该条数据是否一致
                if (rptQingDiaoReportMapper.findIdenticalBySocialCreditCode(rptQingdiaoReportEntityList.get(i)) == null){
                    //如果不一致,添加到updateList集合
                    updateList.add(rptQingdiaoReportEntityList.get(i));
                }else {
                    //如果一致，不执行任何操作
                }
            }
        }

        if (addList.size() > 0){
            //如果addList不为空，执行添加
            rptQingDiaoReportMapper.addQingDiaoReport(addList);
        }
        if (updateList.size() > 0){
            //如果updateList不为空，执行修改
            for (RptQingdiaoReportEntity rptQingdiaoReportEntity : updateList){
                rptQingDiaoReportMapper.updateQingDiaoReport(rptQingdiaoReportEntity);
            }
        }

        return 1;
    }

    /**
     * 添加状态为空号的清吊信息
     * @param rptQingdiaoReportEntity
     * @return
     */
    @Override
    public Integer addQingDiaoReportForEmpty(RptQingdiaoReportEntity rptQingdiaoReportEntity) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String currentDate = simpleDateFormat.format(new Date());

        rptQingdiaoReportEntity.setReportStatus(1);
        rptQingdiaoReportEntity.setCreateTime(currentDate);

        rptQingDiaoReportMapper.addQingDiaoReportForEmpty(rptQingdiaoReportEntity);

        return 1;
    }

    /**
     * 自动在凌晨的时候将当天的清吊报表数据存入到历史表中
     */
    @Scheduled(cron = "0 59 23 * * ? ")
    public void addQingDiaoHistoryReport() {

        //查询税务报表当天的所有数据
        List<RptQingdiaoReportEntity> rptQingdiaoReportEntityList = rptQingDiaoReportMapper.findAll();
        //如果税务报表当天的所有数据存在,就存入历史表中,并且删除当天的数据
        if (rptQingdiaoReportEntityList.size() > 0){
            //添加税务历史报表
            rptQingDiaoHistoryReportMapper.addQingDiaoHistoryReport(rptQingdiaoReportEntityList);
            //删除税务报表所有数据
            rptQingDiaoReportMapper.delQingDiaoReport();
        }
    }





}
