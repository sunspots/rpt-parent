package com.company.project.service.impl;

import com.company.project.entity.RptTaxationReportEntity;
import com.company.project.mapper.RptTaxationHistoryReportMapper;
import com.company.project.mapper.RptTaxationReportMapper;
import com.company.project.service.RptTaxationReportService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class RptTaxationReportServiceImpl implements RptTaxationReportService {

    @Autowired
    private RptTaxationReportMapper rptTaxationReportMapper;

    @Autowired
    private RptTaxationHistoryReportMapper rptTaxationHistoryReportMapper;

    /**
     * 查找税务报表
     * @param rptTaxationReportEntity
     * @return
     */
    @Override
    public PageInfo<RptTaxationReportEntity> findAllTaxationReport(RptTaxationReportEntity rptTaxationReportEntity) {

        PageHelper.startPage(rptTaxationReportEntity.getPage(),rptTaxationReportEntity.getLimit());
        List<RptTaxationReportEntity> list = rptTaxationReportMapper.findAllTaxationReport(rptTaxationReportEntity);
        PageInfo<RptTaxationReportEntity> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    /**
     * 通过社会信用代码查询数据库是否已存在该数据
     * @param socialCreditCode
     * @return
     */
    @Override
    public RptTaxationReportEntity findSocialCreditCodeIsHave(String socialCreditCode) {

        return rptTaxationReportMapper.findSocialCreditCodeIsHave(socialCreditCode);
    }

    /**
     * 通过社会信用代码查询数据库中该条数据是否一致
     * @param rptTaxationReportEntity
     * @return
     */
    @Override
    public RptTaxationReportEntity findIdenticalBySocialCreditCode(RptTaxationReportEntity rptTaxationReportEntity) {

        return rptTaxationReportMapper.findIdenticalBySocialCreditCode(rptTaxationReportEntity);
    }

    /**
     * 添加税务信息
     * @param rptTaxationReportEntityList
     */
    @Override
    public Integer addTaxationReport(List<RptTaxationReportEntity> rptTaxationReportEntityList) {

        rptTaxationReportMapper.addTaxationReport(rptTaxationReportEntityList);

        return 1;
    }

    /**
     * 修改税务信息
     * @param rptTaxationReportEntityList
     */
    @Override
    public Integer updateTaxationReport(List<RptTaxationReportEntity> rptTaxationReportEntityList) {

        for (RptTaxationReportEntity rptTaxationReportEntity : rptTaxationReportEntityList){
            rptTaxationReportMapper.updateTaxationReport(rptTaxationReportEntity);
        }
        return 1;
    }

    /**
     * 自动在凌晨的时候将当天的税务报表数据存入到历史表中
     */
    @Scheduled(cron = "0 59 23 * * ? ")
    public void addTaxationHistoryReport() {

        //查询税务报表当天的所有数据
        List<RptTaxationReportEntity> rptTaxationReportEntityList = rptTaxationReportMapper.findAll();
        //如果税务报表当天的所有数据存在,就存入历史表中,并且删除当天的数据
        if (rptTaxationReportEntityList.size() > 0){
            //删除税务报表所有数据
            rptTaxationReportMapper.delTaxationReport();
            //添加税务历史报表
            rptTaxationHistoryReportMapper.addTaxationHistoryReport(rptTaxationReportEntityList);
        }
    }

    /**
     * 税务报表上传
     * @param upload
     * @return
     * @throws IOException
     */
    @Override
    public List<RptTaxationReportEntity> taxationReportUploadExcel(MultipartFile upload,Integer reportStatus) throws IOException{
        InputStream inputStream = upload.getInputStream();
        List<RptTaxationReportEntity> rptTaxationReportEntityList = new ArrayList<>();
        //获取工作部
        XSSFWorkbook xssfWorkbook = new XSSFWorkbook(inputStream);
        //获取工作表
        XSSFSheet sheet = xssfWorkbook.getSheetAt(0);

        int lastRowNum = sheet.getLastRowNum();
        for (int i = 2; i <= lastRowNum; i++) {
            XSSFRow row = sheet.getRow(i);
            if (row != null) {
                List<String> list = new ArrayList<>();
                //循环遍历每行的单元格
                for (Cell cell : row) {
                    if (cell != null) {
                        String date = "";
                        String value = "";
                        //判断单元格的类型是不是数字类型
                        if (cell.getCellType() == CellType.NUMERIC) {
                            //判断单元格的类型是不是日期类型
                            if (HSSFDateUtil.isCellDateFormatted(cell)) {
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                date = simpleDateFormat.format(cell.getDateCellValue());
                            } else {
                                cell.setCellType(CellType.STRING);
                                value = cell.getStringCellValue();
                            }
                        } else {
                            cell.setCellType(CellType.STRING);
                            value = cell.getStringCellValue();
                        }
                        if (value != null && !"".equals(value)) {
                            list.add(value);
                        } else if (date != null && !"".equals(date)) {
                            list.add(date);
                        }
                    }
                }
                if (list.size() > 0) {
                    RptTaxationReportEntity rptTaxationReportEntity = new RptTaxationReportEntity();

                    String enterpriseName = list.get(1);
                    rptTaxationReportEntity.setEnterpriseName(enterpriseName);//获取企业名称
                    rptTaxationReportEntity.setSocialCreditCode(list.get(2));//获取统一社会信用代码
                    rptTaxationReportEntity.setLegalRepresentative(list.get(3));//法定代表人
                    rptTaxationReportEntity.setBusinessPlace(list.get(4));//经营场所
                    rptTaxationReportEntity.setBusinessScope(list.get(5));//经营范围
                    rptTaxationReportEntity.setContacts(list.get(6));//联系人
                    rptTaxationReportEntity.setContactInformation(list.get(7));//联系方式
                    rptTaxationReportEntity.setStatus(list.get(8));//状态
                    rptTaxationReportEntity.setReportStatus(reportStatus);//数据上报状态
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    String crateTime = simpleDateFormat.format(new Date());
                    rptTaxationReportEntity.setCreateTime(crateTime);//获取当前时间

                    //添加到集合
                    rptTaxationReportEntityList.add(rptTaxationReportEntity);
                }
            }
        }
        return rptTaxationReportEntityList;
    }




}
