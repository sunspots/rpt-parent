package com.company.project.service.impl;

import com.company.project.entity.RptQingdiaoHistoryReportEntity;
import com.company.project.mapper.RptQingDiaoHistoryReportMapper;
import com.company.project.service.RptQingDiaoHistoryReportService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RptQingDiaoHistoryReportServiceImpl implements RptQingDiaoHistoryReportService {

    @Autowired
    private RptQingDiaoHistoryReportMapper rptQingDiaoHistoryReportMapper;

    /**
     * 查询清吊历史报表
     * @param rptQingdiaoHistoryReportEntity
     * @return
     */
    @Override
    public PageInfo<RptQingdiaoHistoryReportEntity> findAllQingDiaoHistoryReport(RptQingdiaoHistoryReportEntity rptQingdiaoHistoryReportEntity) {

        PageHelper.startPage(rptQingdiaoHistoryReportEntity.getPage(), rptQingdiaoHistoryReportEntity.getLimit());
        List<RptQingdiaoHistoryReportEntity> list = rptQingDiaoHistoryReportMapper.findAllQingDiaoHistoryReport(rptQingdiaoHistoryReportEntity);
        PageInfo<RptQingdiaoHistoryReportEntity> pageInfo = new PageInfo<>(list);

        return pageInfo;
    }



}
