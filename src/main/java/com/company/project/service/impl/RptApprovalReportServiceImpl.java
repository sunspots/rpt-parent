package com.company.project.service.impl;

import com.company.project.entity.RptApprovalReportEntity;
import com.company.project.mapper.RptApprovalHistoryReportMapper;
import com.company.project.mapper.RptApprovalReportMapper;
import com.company.project.service.RptApprovalReportService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class RptApprovalReportServiceImpl implements RptApprovalReportService {

    @Autowired
    private RptApprovalReportMapper rptApprovalReportMapper;

    @Autowired
    private RptApprovalHistoryReportMapper rptApprovalHistoryReportMapper;

    /**
     * 查询核准报表
     * @param rptApprovalReportEntity
     * @return
     */
    @Override
    public PageInfo<RptApprovalReportEntity> findAllApprovalReport(RptApprovalReportEntity rptApprovalReportEntity) {

        PageHelper.startPage(rptApprovalReportEntity.getPage(), rptApprovalReportEntity.getLimit());
        List<RptApprovalReportEntity> list = rptApprovalReportMapper.findAllApprovalReport(rptApprovalReportEntity);
        PageInfo<RptApprovalReportEntity> pageInfo = new PageInfo<>(list);

        return pageInfo;
    }

    /**
     * 查询核准报表所有数据
     * @return
     */
    @Override
    public List<RptApprovalReportEntity> findAll() {

        return rptApprovalReportMapper.findAll();
    }

    /**
     * 添加核准报表
     * @param rptApprovalReportEntityList
     * @return
     */
    @Override
    public Integer addApprovalReport(List<RptApprovalReportEntity> rptApprovalReportEntityList) {

        rptApprovalReportMapper.addApprovalReport(rptApprovalReportEntityList);

        return 1;
    }

    /**
     * 修改核准报表
     * @param rptApprovalReportEntity
     * @return
     */
    @Override
    public Integer updateApprovalReport(RptApprovalReportEntity rptApprovalReportEntity) {

        rptApprovalReportMapper.updateApprovalReport(rptApprovalReportEntity);

        return 1;
    }

    /**
     * 添加核准历史报表
     * @param rptApprovalReportEntityList
     * @return
     */
    @Override
    public Integer addApprovalHistoryReport(List<RptApprovalReportEntity> rptApprovalReportEntityList) {

        rptApprovalHistoryReportMapper.addApprovalHistoryReport(rptApprovalReportEntityList);

        return 1;
    }

    /**
     * 通过id查询核准报表
     * @param approvalReportId
     * @return
     */
    @Override
    public RptApprovalReportEntity findByIdApprovalReport(Integer approvalReportId) {

        return rptApprovalReportMapper.findByIdApprovalReport(approvalReportId);
    }

    /**
     * 删除核准报表
     * @param ids
     * @return
     */
    @Override
    public Integer delApprovalReport(List<Integer> ids) {

        rptApprovalReportMapper.delApprovalReport(ids);

        return 1;
    }

    /**
     * 删除核准报表所有数据
     * @return
     */
    @Override
    public Integer delAllApprovalReport() {

        return rptApprovalReportMapper.delAllApprovalReport();
    }

    /**
     * 核准报表Excel上传
     * @param upload
     * @return
     * @throws IOException
     */
    @Override
    public List<RptApprovalReportEntity> approvalReportUploadExcel(MultipartFile upload) throws IOException {

        InputStream inputStream = upload.getInputStream();
        List<RptApprovalReportEntity> rptApprovalReportEntityList = new ArrayList<>();

        //获取工作簿
        XSSFWorkbook xssfWorkbook = new XSSFWorkbook(inputStream);
        //获取工作表
        XSSFSheet sheet = xssfWorkbook.getSheetAt(0);

        int lastRowNum = sheet.getLastRowNum();
        for (int i = 2; i <= lastRowNum; i++) {
            XSSFRow row = sheet.getRow(i);
            if (row != null) {
                List<String> list = new ArrayList<>();
                //循环遍历每行的单元格
                for (Cell cell : row) {
                    if (cell != null) {
                        String date = "";
                        String value = "";
                        //判断单元格的类型是不是数字类型
                        if (cell.getCellType() == CellType.NUMERIC){
                            //判断单元格的类型是不是日期类型
                            if (HSSFDateUtil.isCellDateFormatted(cell)){
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                date = simpleDateFormat.format(cell.getDateCellValue());
                            }else {
                                cell.setCellType(CellType.STRING);
                                value = cell.getStringCellValue();
                            }
                        } else {
                            cell.setCellType(CellType.STRING);
                            value = cell.getStringCellValue();
                        }
                        if (value != null && !"".equals(value)) {
                            list.add(value);
                        }else if (date != null && !"".equals(date)){
                            list.add(date);
                        }
                    }
                }
                if (list.size() > 0) {
                    RptApprovalReportEntity rptApprovalReportEntity = new RptApprovalReportEntity();

                    rptApprovalReportEntity.setRegistrationAuthority(list.get(1));//获取登录机关
                    rptApprovalReportEntity.setDomination(list.get(2));//获取管辖机关
                    rptApprovalReportEntity.setAddress(list.get(3));//获取地址
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    String crateTime = simpleDateFormat.format(new Date());
                    rptApprovalReportEntity.setCreateTime(crateTime);//获取当前时间

                    //添加到集合
                    rptApprovalReportEntityList.add(rptApprovalReportEntity);
                }
            }
        }

        return rptApprovalReportEntityList;
    }




}
