package com.company.project.service.impl;

import com.company.project.entity.RptTaxationHistoryReportEntity;
import com.company.project.mapper.RptTaxationHistoryReportMapper;
import com.company.project.service.RptTaxationHistoryReportService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RptTaxationHistoryReportServiceImpl implements RptTaxationHistoryReportService {

    @Autowired
    private RptTaxationHistoryReportMapper rptTaxationHistoryReportMapper;

    /**
     * 查询税务历史报表
     * @param rptTaxationHistoryReportEntity
     * @return
     */
    @Override
    public PageInfo<RptTaxationHistoryReportEntity> findAllTaxationHistoryReport(RptTaxationHistoryReportEntity rptTaxationHistoryReportEntity) {

        PageHelper.startPage(rptTaxationHistoryReportEntity.getPage(), rptTaxationHistoryReportEntity.getLimit());
        List<RptTaxationHistoryReportEntity> list = rptTaxationHistoryReportMapper.findAllTaxationHistoryReport(rptTaxationHistoryReportEntity);
        PageInfo<RptTaxationHistoryReportEntity> pageInfo = new PageInfo<>(list);

        return pageInfo;
    }



}
