package com.company.project.service;

import com.company.project.entity.RptApprovalReportEntity;
import com.github.pagehelper.PageInfo;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface RptApprovalReportService {

    //查询核准报表
    PageInfo<RptApprovalReportEntity> findAllApprovalReport(RptApprovalReportEntity rptApprovalReportEntity);

    //查询核准报表所有数据
    List<RptApprovalReportEntity> findAll();

    //添加核准报表
    Integer addApprovalReport(List<RptApprovalReportEntity> rptApprovalReportEntityList);

    //修改核准报表
    Integer updateApprovalReport(RptApprovalReportEntity rptApprovalReportEntity);

    //添加核准历史报表
    Integer addApprovalHistoryReport(List<RptApprovalReportEntity> rptApprovalReportEntityList);

    //通过id查询核准报表
    RptApprovalReportEntity findByIdApprovalReport(Integer approvalReportId);

    //删除核准报表
    Integer delApprovalReport(List<Integer> ids);

    //删除核准报表所有数据
    Integer delAllApprovalReport();

    //核准报表Excel上传
    List<RptApprovalReportEntity> approvalReportUploadExcel(MultipartFile upload) throws IOException;




}
