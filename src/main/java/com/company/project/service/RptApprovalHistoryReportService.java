package com.company.project.service;

import com.company.project.entity.RptApprovalHistoryReportEntity;
import com.github.pagehelper.PageInfo;

public interface RptApprovalHistoryReportService {

    //查询核准历史报表
    PageInfo<RptApprovalHistoryReportEntity> findAllApprovalHistoryReport(RptApprovalHistoryReportEntity rptApprovalHistoryReportEntity);

}
