package com.company.project.service;

import com.company.project.entity.RptQingdiaoHistoryReportEntity;
import com.github.pagehelper.PageInfo;

public interface RptQingDiaoHistoryReportService {

    //查询清吊历史报表
    PageInfo<RptQingdiaoHistoryReportEntity> findAllQingDiaoHistoryReport(RptQingdiaoHistoryReportEntity rptQingdiaoHistoryReportEntity);

}
