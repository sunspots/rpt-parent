package com.company.project.service;

import com.company.project.entity.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public interface ExcelDownloadService {

    void matchingDownload(String title, List<RptMatchingReportEntity> list, HttpServletResponse response) throws IOException;

    void statisticsReportExcel(String title, List<RptStatisticsReportEntity> list, HttpServletResponse response);

    void statisticsReportExcel1(String title, List<RptStatisticsReportEntity> list, HttpServletResponse response);

    void matchingDownloadEmpty(String title, List<RptQingdiaoReportEntity> list, HttpServletResponse response);

    void annualHistoryDownload(String fileName, List<RptAnnualHistoryReportEntity> rptAnnualHistoryReportEntityList, HttpServletResponse response);

    void rptApprovalHistoryDownload(String fileName, List<RptApprovalHistoryReportEntity> rptApprovalHistoryReportEntityList, HttpServletResponse response);

    void rptTaxationHistoryDownload(String fileName, List<RptTaxationHistoryReportEntity> list, HttpServletResponse response);

    void matchingHistoryDownload(String fileName, List<RptMatchingHistoryReportEntity> rptMatchingHistoryReportEntityList, HttpServletResponse response);

    void qingDiaoHistoryDownload(String fileName, List<RptQingdiaoHistoryReportEntity> rptQingdiaoHistoryReportEntityList, HttpServletResponse response);
}
