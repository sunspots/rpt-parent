package com.company.project.service;

import com.company.project.entity.RptStatisticsHistoryReportEntity;
import com.company.project.entity.RptStatisticsReportEntity;
import com.github.pagehelper.PageInfo;
import org.openxmlformats.schemas.drawingml.x2006.chart.STRadarStyle;

import java.util.List;
import java.util.Map;

public interface RptStatisticsReportService {

    // 通过街道地址查询所有街道的年报数量
    Map<String ,Object> findAddressAnnualReportByAddress();

    // 通过地址查询所有街道的年报数量（包括清吊和空号）
    Map<String,Object> findEmptyAndClearNumberReportByAddress();

    Map<String,Object> findAllStatisticsReport();

    PageInfo<RptStatisticsHistoryReportEntity> findStatisticsHistory(RptStatisticsHistoryReportEntity rptStatisticsHistoryReportEntity);

    PageInfo<RptStatisticsHistoryReportEntity> findStatisticsHistory1(RptStatisticsHistoryReportEntity rptStatisticsHistoryReportEntity);

}
