package com.company.project.service;

import com.company.project.entity.RptRobotProblemEntity;

import java.util.List;

public interface RptRobotProblemService {

    Integer addProblem(RptRobotProblemEntity problem);

    Integer updateProblem(RptRobotProblemEntity problem);

    RptRobotProblemEntity getProblemById(Integer id);

    List<RptRobotProblemEntity> getAllProblem();

    List<RptRobotProblemEntity> getProblem(RptRobotProblemEntity problem);
}
