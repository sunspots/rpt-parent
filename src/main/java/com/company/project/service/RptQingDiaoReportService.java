package com.company.project.service;

import com.company.project.entity.RptQingdiaoReportEntity;
import com.github.pagehelper.PageInfo;

import java.text.ParseException;
import java.util.List;

public interface RptQingDiaoReportService {

    //查询清吊报表
    PageInfo<RptQingdiaoReportEntity> findAllQingDiaoReport(RptQingdiaoReportEntity rptQingdiaoReportEntity);

    //通过社会信用代码查询在数据库是否已存在
    RptQingdiaoReportEntity findSocialCreditCodeIsHave(String socialCreditCode);

    //通过社会信用代码查询数据库中该条数据是否一致
    RptQingdiaoReportEntity findIdenticalBySocialCreditCode(RptQingdiaoReportEntity rptQingdiaoReportEntity);

    //点击触发清吊事件
    Integer triggerQingDiaoForTaxationReport() throws ParseException;

    //添加状态为空号的清吊信息
    Integer addQingDiaoReportForEmpty(RptQingdiaoReportEntity rptQingdiaoReportEntity);




}
