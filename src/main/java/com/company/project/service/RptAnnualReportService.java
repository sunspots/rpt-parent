package com.company.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.company.project.entity.RptAnnualReportEntity;
import com.company.project.entity.RptMatchingReportEntity;
import com.company.project.entity.RptStatisticsReportEntity;
import com.github.pagehelper.PageInfo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public interface RptAnnualReportService extends IService<RptAnnualReportEntity> {

    //查询工商报表
    PageInfo<RptAnnualReportEntity> pageInfo(RptAnnualReportEntity rptAnnualReportEntity);

    //通过社会信用代码查询数据库是否已存在该数据
    RptAnnualReportEntity findSocialCreditCodeIsHave(String socialCreditCode);

    //通过社会信用代码查询数据库中该条数据是否一致
    RptAnnualReportEntity findIdenticalBySocialCreditCode(RptAnnualReportEntity rptAnnualReportEntity);

    //添加工商报表
    Integer addAnnualReport(List<RptAnnualReportEntity> rptAnnualReportEntityList);

    //修改工商报表
    Integer updateAnnualReport(List<RptAnnualReportEntity> rptAnnualReportEntityList);

    //工商报表Excel上传
    List<RptAnnualReportEntity> annualReportUploadExcel(MultipartFile upload,Integer reportStatus) throws IOException;

    //工商报表Excel下载
    Integer annualReportExcel(String title, List<RptAnnualReportEntity> rptAnnualReportEntityList, HttpServletResponse response);

    void excelFile(HttpServletResponse response) throws IOException;


    void exportInfo(String title, List<RptStatisticsReportEntity> list, HttpServletResponse response) throws IOException;

    List<RptAnnualReportEntity> findAll();

    //void matchingReportDownloadExcel(String , List<RptMatchingReportEntity> , HttpServletResponse );
    //void exportInfo(String title, List<RptAnnualReportEntity> rptAnnualReportEntity, HttpServletResponse response) throws IOException ;
}
