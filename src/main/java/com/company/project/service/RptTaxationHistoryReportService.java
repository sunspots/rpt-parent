package com.company.project.service;

import com.company.project.entity.RptTaxationHistoryReportEntity;
import com.github.pagehelper.PageInfo;

public interface RptTaxationHistoryReportService {

    //查询税务历史报表
    PageInfo<RptTaxationHistoryReportEntity> findAllTaxationHistoryReport(RptTaxationHistoryReportEntity rptTaxationHistoryReportEntity);


}
